classdef PlotStyleTestCase < matlab.unittest.TestCase
  %% PLOTSTYLETESTCASE
  
  
  
  %% TEST PARAMETERS
  properties ( TestParameter )
    
    style = { ...
        BeamerMTheme169PlotStyle.OneOne ...
      , BeamerMTheme169PlotStyle.OneHalf ...
      , BeamerMTheme169PlotStyle.OneThird ...
      , BeamerMTheme169PlotStyle.TwoThird ...
    }
    
  end
  
  
  
  %% TEST METHODS
  methods ( Test )
    
    function singleFigure(obj, style)
      %% SINGLEFIGURE
      
      
      
      hf = figure();
      hax = axes(hf);
      
      x = reshape(linspace(0, 2 * pi, 361), [], 1);
      y = cat(2, sin(x), cos(x), atan(x));
      
      plot( ...
          hax ...
        , x, y ...
      );
      
      legend(hax);
      
      apply(style, hf);
      hf.Name = last(strsplit(class(style), '.'));
      
    end
    
  end
  
end
