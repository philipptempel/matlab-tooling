classdef PlotStyle < matlab.mixin.SetGet
  %% PLOTSTYLE
  
  
  
  %% READ-ONLY PROPERTIES
  properties ( SetAccess = protected )
    
    Version         (1, 1) double             = 1
    Format          (1, :) char               = 'eps'
    Preview         (1, :) char               = 'none'
    Width           (1, :) { double, char }   = 10
    Height          (1, :) { double, char }   = 'auto'
    Units           (1,: ) char               = 'centimeters'
    Color           (1, :) char               = 'rgb'
    Background      (1, :) double             = []
    FixedFontSize   (1, :) double             = 10
    ScaledFontSize  (1, 1) logical            = true
    FontMode        (1, :) char               = 'scaled'
    FontSizeMin     (1, 1) double             = 8
    FixedLineWidth  (1, 1) logical            = false
    ScaledLineWidth (1, 1) logical            = true
    LineMode        (1, :) char               = 'scaled'
    LineWidthMin    (1, 1) double             = 1.00
    FontName        (1, :) char               = 'auto'
    FontWeight      (1, :) char               = 'auto'
    FontAngle       (1, :) { char , double }  = 'auto'
    FontEncoding    (1, :) char               = 'latin1'
    PSLevel         (1, 1) double             = 3
    Renderer        (1, :) char               = 'painters'
    Resolution      (1, 1) double             = 150
    LineStyleMap    (1, :) char               = 'none'
    ApplyStyle      (1, 1) logical            = true
    Bounds          (1, :) char               = 'loose'
    LockAxes        (1, 1) logical            = true
    LockAxesTick    (1, 1) logical            = true
    ShowUI          (1, 1) logical            = true
    SeparateText    (1, 1) logical            = true
    
  end
  
  
  
  %% PUBLIC METHODS
  methods ( Sealed )
    
    function apply(obj, hf)
      %% APPLY
      
      
      
      narginchk(1, 2);
      
      % APPLY(OBJ)
      if nargin < 2 || isempty(hf)
        hf = gcf();
      end
      
      % Convert width and height from export style char to numeric values
      fw  = obj.Width;
      fh  = obj.Height;
      dpi = obj.Resolution;
      stu = obj.Units;
      
      % All our algorithms are based on working in centimeters
      units  = 'centimeters';
      
      % If target units is POINTS, we will convert them to CM
      if strcmp(stu, 'points')
        % If both target width and target height are NaN, we will get the figure's
        % current width and height in the target units
        if isempty(fw) && isempty(fh)
          stwh = getfigwhin(hf, stu);
        
        % Width is "AUTO", height not
        elseif isempty(fw) && ~isempty(fh)
          [fw, ~] = getfigwhin(hf, stu);
          
        % Height is "AUTO", width not
        elseif ~isempty(fw) && isempty(fh)
          [~, fh] = getfigwhin(hf, stu);
          
        end
        
        % Convert width and height from POINTS to CENTIMETER
        stwh = pts2cm(stwh, dpi);
        
      % If target units is INCHES, we will convert them to CM
      elseif strcmp(stu, 'inches')
        % If both target width and target height are NaN, we will get the figure's
        % current width and height in the target units
        if isempty(fw) && isempty(fh)
          stwh = getfigwhin(hf, stu);
        
        % Width is "AUTO", height not
        elseif isempty(fw) && ~isempty(fh)
          [fw, ~] = getfigwhin(hf, stu);
          
        % Height is "AUTO", width not
        elseif ~isempty(fw) && isempty(fh)
          [~, fh] = getfigwhin(hf, stu);
          
        end
        
        % Convert width and height from INCHES to CENTIMETERS
        stwh = in2cm(stwh);
        
      else
        % If both target width and target height are NaN, we will get the figure's
        % current width and height in the target units
        if isempty(fw) && isempty(fh)
          stwh = getfigwhin(hf, stu);
        
        % Width is "AUTO", height not
        elseif isempty(fw) && ~isempty(fh)
          [fw, ~] = getfigwhin(hf, stu);
          
        % Height is "AUTO", width not
        elseif ~isempty(fw) && isempty(fh)
          [~, fh] = getfigwhin(hf, stu);
          
        end
        
      end
      
      % Get full style definition
      stl = struct(obj);
      stl.Width = fw;
      stl.Height = fh;
      
      % Apply style to figure
      hgexport( ...
          hf ...
        , tempname() ...
        , stl ...
        , 'ApplyStyle', true ...
      );
      
      % Get figure width and height in correct units
      fp = hf.Position;
      fp([3,4]) = [ fw , fh ];
      
      % Set print paper size from the style and from above calculated values
      set( ...
          hf ...
        , 'PaperPositionMode' , 'manual' ...
        , 'PaperUnits'        , units ...
        , 'PaperSize'         , stwh ...
        , 'PaperPosition'     , [ 0 , 0 , fw , fh ] ...
        , 'Position'          , fp ...
      );
      
      if isempty(hf.UserData)
        hf.UserData = struct();
      end
      hf.UserData.Resolution = dpi;
      
      
    end
    
  end
  
  
  
  %% CONVERSION METHODS
  methods
    
    function s = struct(obj)
      %% STRUCT
      
      
      
      s = struct();
      
      s.Version = obj.Version;
      s.Format = obj.Format;
      s.Preview = obj.Preview;
      s.Width = obj.Width;
      s.Height = obj.Height;
      s.Units = obj.Units;
      s.Color = obj.Color;
      s.Background = obj.Background;
      s.FixedFontSize = obj.FixedFontSize;
      s.ScaledFontSize = obj.ScaledFontSize;
      s.FontMode = obj.FontMode;
      s.FontSizeMin = obj.FontSizeMin;
      s.FixedLineWidth = obj.FixedLineWidth;
      s.ScaledLineWidth = obj.ScaledLineWidth;
      s.LineMode = obj.LineMode;
      s.LineWidthMin = obj.LineWidthMin;
      s.FontName = obj.FontName;
      s.FontWeight = obj.FontWeight;
      s.FontAngle = obj.FontAngle;
      s.FontEncoding = obj.FontEncoding;
      s.PSLevel = obj.PSLevel;
      s.Renderer = obj.Renderer;
      s.Resolution = obj.Resolution;
      s.LineStyleMap = obj.LineStyleMap;
      s.ApplyStyle = obj.ApplyStyle;
      s.Bounds = obj.Bounds;
      s.LockAxes = obj.LockAxes;
      s.LockAxesTick = obj.LockAxesTick;
      s.ShowUI = obj.ShowUI;
      s.SeparateText = obj.SeparateText;
      
    end
    
  end
  
end
