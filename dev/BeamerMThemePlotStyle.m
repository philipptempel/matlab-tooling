classdef BeamerMThemePlotStyle < PlotStyle
  %% BEAMERMTHEMEPLOTSTYLE
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = BeamerMThemePlotStyle()
      %% BEAMERMTHEMEPLOTSTYLE
      
      
      
      obj@PlotStyle();
      
      obj.Format        = 'eps';
      obj.Units         = 'centimeters';
      obj.FontSizeMin   = 8;
      obj.LineWidthMin  = 1.00;
      
    end
    
  end
  
end
