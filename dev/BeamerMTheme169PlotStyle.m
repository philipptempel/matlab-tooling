classdef BeamerMTheme169PlotStyle < BeamerMThemePlotStyle
  %% BEAMERMTHEME169PLOTSTYLE
  
  
  
  %% ENUMERATION TYPES
  enumeration
    
    OneOne    (14.0000, [])
    
    OneHalf   (6.82428, [])
    
    OneThird  (4.44429, [])
    
    TwoThird  (9.06433, [])
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = BeamerMTheme169PlotStyle(w, h)
      %% BEAMERMTHEME169PLOTSTYLE
      
      
      
      obj@BeamerMThemePlotStyle();
      
      obj.Width  = w;
      obj.Height = h;
      
    end
    
  end
  
end
