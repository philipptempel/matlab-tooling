classdef PlotThemeTestCase < matlab.unittest.TestCase
  %% PLOTTHEMETESTCASE
  
  
  
  %% TEST PARAMETERS
  properties ( TestParameter )
    
    theme = { ...
        SolarizedPlotTheme.Light ...
      , SolarizedPlotTheme.Dark ...
      , BeamerMThemePlotTheme.Light ...
      , BeamerMThemePlotTheme.Dark ...
    }
    
  end
  
  
  
  %% TEST METHODS
  methods ( Test )
    
    function singleFigure(obj, theme)
      %% SINGLEFIGURE
      
      
      
      hf = figure();
      hax = axes(hf);
      
      x = reshape(linspace(0, 2 * pi, 361), [], 1);
      y = cat(2, sin(x), cos(x), atan(x));
      
      plot( ...
          hax ...
        , x, y ...
      );
      
      legend(hax);
      
      apply(theme, hf);
      hf.Name = last(strsplit(class(theme), '.'));
      
    end
    
  end
  
end
