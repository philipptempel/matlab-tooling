function test_baryinterp()
%% TEST_BARYINTERP



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

narginchk(0, 0);

nargoutchk(0, 0);



%% Test with vector function y = f(x)

nx = 100;
nq = 250;
x = cos( pi * transpose(0:nx) / nx );
y = abs(x) + 0.5 .* x - x .^ 2;

xq = linspace(-1, 1, nq);
yq = baryinterp(x, y, xq);

hf = figure();
hax = axes('Parent', hf);
plot(x, y, 'LineStyle', 'none', 'Marker', '.');
hold('on');
plot(xq, yq, 'LineStyle', '-', 'Marker', '.')
colororder(hax, rand(size(y, 2), 3));



%% Test with matrix function Y = F(x)

nx = 100;
nq = 250;
ny = 3;
x = cos( pi * transpose(0:nx) / nx );
y = abs(x) + 0.5 .* x - x .^ 2;
y = y .* exp(0:(ny - 1));

xq = linspace(-1, 1, nq);
yq = baryinterp(x, y, xq);

hf = figure();
hax = axes('Parent', hf);
plot(x, y, 'LineStyle', 'none', 'Marker', '.');
hold('on');
plot(xq, yq, 'LineStyle', '-', 'Marker', '.')
colororder(hax, rand(size(y, 2), 3));



end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
