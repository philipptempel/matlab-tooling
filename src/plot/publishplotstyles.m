function publishplotstyles(cwd)
%% PUBLISHPLOTSTYLES Publish plot styles to MATLAB's `ExportSetup` directory
%
% PUBLISHPLOTSTYLES() publishes plot styles to MATLAB's `ExportSetup` directory
% from the current working directory. Plot styles are saved in standard `.txt`
% files.
%
% PUBLISHPLOTSTYLES(CWD) publishes plot styles found in the given directory.
%
% Inputs:
%
%   WD                      Path to directory in which the plot styles resides.
%                           Defaults to current working directory.



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-02-02
% Changelog:
%   2023-02-02
%       * Initial release



%% Parse arguments

% PUBLISHPLOTSTYLES()
% PUBLISHPLOTSTYLES(CWD)
narginchk(0, 1);

% PUBLISHPLOTSTYLES(___)
nargoutchk(0, 0);

% PUBLISHPLOTSTYLES()
if nargin < 1 || isempty(cwd)
  cwd = pwd();
end



%% Algorithm

% Check if we have plot styles in the current directory
pst = allfiles(cwd, '.txt', 'Recurse', 'off');
npst = numel(pst);

% No plot styles in the current directory, so let us find them in a `plotstyles`
% subdirectory
if npst == 0
  cwd = fullfile(cwd, 'plotstyles');
  pst = allfiles(cwd, '.txt', 'Recurse', 'on');
  npst = numel(pst);
  
end

% Ensure the `ExportSetup` directory exists
ensuredir(fullfile(prefdir(0), 'ExportSetup'));

% Copy each plot style
for ipst = 1:npst
  pst_ = pst(ipst);
  reldir = strip(strrep(pst_.folder, cwd, ''), filesep());
  tdir = ensuredir(fullfile(prefdir(0), 'ExportSetup', reldir));
  copyfile(fullfile(pst_.folder, pst_.name), fullfile(tdir, pst_.name));
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
