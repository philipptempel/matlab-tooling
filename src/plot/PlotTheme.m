classdef PlotTheme < matlab.mixin.SetGet
  %% PLOTTHEME
  
  
  
  %% PUBLIC METHODS
  methods ( Sealed )
    
    function apply(obj, gob)
      %% APPLY
      
      
      % APPLY(OBJ)
      % APPLY(OBJ, FIG)
      narginchk(1, 2);
      
      % APPLY(___)
      nargoutchk(0, 0);
      
      % APPLY(OBJ)
      if nargin < 2 || isempty(gob)
        gob = gcf();
      end
      
      % Style figure
      setGroup(obj, gob, 'Figure');
      
      % Get all axes objects
      hax = findobj(gob, 'type', 'axes');
      
      % Ensure we found some axes
      if ~isempty(hax)
        % Get legend object
        hll = [ hax.Legend ];
        % Get all numeric ruler objects
        hxx = [ hax.XAxis ];
        hxy = [ hax.YAxis ];
        hxz = [ hax.ZAxis ];
        
        % Set groups of theme styles
        setGroup(obj, hax, 'Axes');
        setGroup(obj, [ hxx , hxy , hxz ], 'Axis');
        setGroup(obj, hxx, 'XAxis');
        setGroup(obj, hxy, 'YAxis');
        setGroup(obj, hxz, 'ZAxis');
        setGroup(obj, hll, 'Legend');
      end
      
    end
    
  end
  
  
  
  %% INTERNAL METHODS
  methods ( Access = protected )
    
    function setGroup(obj, gob, group)
      %% SETGROUP
      
      
      
      % Create the property key from the group
      pk = [ group , '_' ];
      
      % Get properties of theme
      pl = meta.class.fromName(class(obj)).PropertyList;
      % Remove any non-public properties
      pl(~strcmp({ pl.GetAccess }, 'public')) = [];
      
      % Get properties to set and count them
      ks = { pl(startsWith({ pl.Name }, pk)).Name };
      nks = numel(ks);
      
      % If there are properties to set
      if nks > 0
        % Loop over each key and split it into its components
        for ik = 1:nks
          % Get value
          v = obj.(ks{ik});
          % Get key
          k = strrep(ks{ik}, pk, '');
          % Object to assign value to
          p = gob;
          % Split key into its sub-kys
          kk = strsplit(k, '_');
          
          % Get first key
          k_ = kk{1};
          kk = kk(2:end);
          % While there are more sub keys, we will update the parent accordingly
          while ~isempty(kk)
            p = p.(k_);
            k_ = kk{1};
            kk = kk(2:end);
          end
          
          % Finally, set key/value pair on the correct parent object
          set(p, k_, v);
          
        end
        
      end
      
    end
    
  end
  
end
