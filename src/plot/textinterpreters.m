function textinterpreters(intp, varargin)
%% TEXTINTERPRETERS Set default text interpreters on graphics object
%
% TEXTINTERPRETERS(INTP)
%
% TEXTINTERPRETERS(H, INTP)
%
% Inputs:
% 
%   INTP                    Interpreter name.
%
%   H                       Graphics object handle.



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2022-04-11
% Changelog:
%   2022-04-11
%       * Initial release



%% Parse arguments

% TEXTINTERPRETERS(INTP)
% TEXTINTERPRETERS(H, INTP)
narginchk(1, 2);

% TEXTINTERPRETERS(___)
nargoutchk(0, 0);

% TEXTINTERPRETERS(INTP)
if nargin < 2
  h = findall(gca());

% TEXTINTERPRETERS(H, INTP)
else
  % Pop off graphics objects and the interpreter tag
  h = intp;
  intp = varargin{1};
  varargin(1) = [];
  
end



%% Algorithm

% Find all unique child objects of the given object
ha = unique(findall(h));
nhs = numel(ha);

% Loop over each object...
for ih = 1:nhs
  % Get object and its public properties
  h_ = ha(ih);
  p = properties(h_);
  
  % See if the object has any property ending in "Interpreter"
  pmatch = p(endsWith(p, 'Interpreter', 'IgnoreCase', true));
  if ~isempty(pmatch)
    % Set interpreter for the matching properties
    set(h_, pmatch, repmat({ intp }, size(pmatch)));

  end

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
