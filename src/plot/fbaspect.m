function fbaspect(far, varargin)%#codegen
%% FBASPECT Set figure box aspect ratio
%
% FBASPECT([W H]) sets the current figure's aspect ratio to W:H such that the
% height remains unchanged and the width satisfies the aspect ratio.
%
% FBASPECT(FIG, ...) sets aspect ratio of figure given in handle FIG to W:H.
%
% Inputs:
%
%   VAL                     Scalar value or W/H or 1x2 vector of apsect ration
%                           given as [ W , H ].
%
% See also:
%   DASPECT PBASPECT



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2022-09-16
% Changelog:
%   2022-09-16
%       * Initial release



%% Parse arguments

% FBASPECT(VAL)
% FBASPECT(HF, VAL)
narginchk(1, 2);

% FBASPECT(__)
nargoutchk(0, 0);

% Get figure object (if any)
[hf, args] = figcheck(far, varargin{:});
% No figure?
if isempty(hf)
  % Get current figure
  hf = gcf();
end

% Pop off aspect ratio
far = args{1};
args(1) = [];



%% Algorithm

% Turn vector of aspect ratio to scalar value
if ~isscalar(far)
  far = far(1) / far(2);
end

% Set units to pixels
ounits = hf.Units;
hf.Units = 'pixels';

% Get current figure size i.e., position
fp = hf.InnerPosition;

% New position array
pnew = fp;
pnew(3) = fp(3);
pnew(4) = pnew(3) / far;
% Calculate change in height so we move the figure up i.e., make it resize WRT
% its top left corner
hdiff = pnew(4) - fp(4);
pnew(2) = pnew(2) - hdiff;

% Check if width is wider than the width of the screen the figure is on
ssz = screensize();
pnew([1,2]) = limit(pnew([1,2]), [1,1], ssz - pnew([3,4]));

% Set figure position according to new aspect ratio
hf.InnerPosition = pnew;
% Revert units of figure
hf.Units = ounits;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
