function hf = reusefigure(varargin)
%% REUSEFIGURE Reuse a figure or create new one
%
% HF = REUSEFIGURE() reuses the current figure if there is any otherwise creates
% a new figure.
%
% HF = REUSEFIGURE(Name, Value, ...) finds figure matching _ALL_ of the
% properties given in Name/Value pairs. If no figure can be found, a new figure
% is created.
%
% Outputs:
%
%   HF                      Figure handle.
%
% See also:
%   FIGURE FINDOBJ



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-04-21
% Changelog:
%   2023-04-21
%       * Initial release



%% Parse arguments

% REUSEFIGURE()
% REUSEFIGURE(Name, Value)
narginchk(0, Inf);

% REUSEFIGURE(___)
% HF = REUSEFIGURE(___)
nargoutchk(0, 1);



%% Algorithm

% If arguments are given, try to find the matching figure
if nargin > 0
  % Find a figure matching all properties given to the function call
  hf = findobj(groot(), 'type', 'figure', varargin{:});
  
  % No figure found...
  if isempty(hf)
    % So create a new figure
    hf = figure(varargin{:});
    
  end
  
% No arguments given, so `REUSEFIGURE(___)` should act like `FIGURE(___)`
else
  % Act like `FIGURE()`
  hf = figure();
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
