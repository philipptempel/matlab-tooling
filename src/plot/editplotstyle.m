function editplotstyle(ps)
%% EDITPLOTSTYLE Edit a plot style from `ExportSetup`
%
% EDITPLOTSTYLE(PLOTSTYLE) opens the plot style PLOTSTYLE in MATLAB's editor.
%
% Inputs:
%
%   PLOTSTYLE               Name of plot style as found in the user's
%                           `ExportSetup` directory.
%
% See also:
%   HGEXPORT



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% EDITPLOTSTYLE(PLOTSTYLE)
narginchk(1, 1);

% EDITPLOTSTYLE(___)
nargoutchk(0, 0);



%% Algorithm

edit(fullfile(prefdir(0), 'ExportSetup', sprintf('%s.txt', ps)));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
