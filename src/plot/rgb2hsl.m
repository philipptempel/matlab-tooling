function hsl = rgb2hsl(rgb)
%% RGB2HSL
%
% HSL = RGB2HSL(RGB)
%
% Inputs:
%
%   RGB                     Description of argument RGB
%
% Outputs:
%
%   HSL                     Nx3 array of HSL values in integer range



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-05-30
% Changelog:
%   2023-05-30
%       * Initial release



%% Parse arguments

% RGB2HSL(RGB)
narginchk(1, 1);

% RGB2HSL(___)
% HSL = RGB2HSL(___)
nargoutchk(0, 1);



%% Algorithm

% Ensure RGB is double values
rgb = double(rgb);

% Flag whether values are given as integers in 1...255 or not
isinint = any(rgb(:) > 1);

% Convert RGB values into range of 0...1
if isinint
  rgb = rgb ./ 255;
  
end

% Split RGB into R, G, and B
R = rgb(:,1);
G = rgb(:,2);
B = rgb(:,3);

% Minimal and maximal color values
Cmin = min(rgb, [], 2);
Cmax = max(rgb, [], 2);

% Difference between maximum and minimum values
dlta = Cmax - Cmin;

% Initialize array
hsl = zeros(size(rgb, 1), 3);

% Calculate lightness
hsl(:,3) = ( Cmax + Cmin ) ./ 2;

% Calculate saturation
indNonsing        = ~isclose(dlta, 0);
hsl(indNonsing,2) = dlta(indNonsing) ./ ( 1 - abs( 2 * hsl(:,3) - 1));

% Calculate hue
indMaxRed   = isclose(Cmax, R);
indMaxGreen = isclose(Cmax, G);
indMaxBlue  = isclose(Cmax, B);
hsl(indMaxRed,1)   = mod( ( G(indMaxRed)   - B(indMaxRed)   ) ./ dlta(indMaxRed)      , 6);
hsl(indMaxGreen,1) = 60 *    ( 2 + ( B(indMaxGreen) - R(indMaxGreen) ) ./ dlta(indMaxGreen)   );
hsl(indMaxBlue,1)  = 60 *    ( 4 + ( R(indMaxBlue)  - G(indMaxBlue)  ) ./ dlta(indMaxBlue)    );

% Scaling of HUE from 0...360 to 0...1
hsl(:,1) = hsl(:,1) ./ 360;
% Ensuring division by 0 doesn't yield NaN or InF
hsl(isnan(hsl(:,1)) | isinf(hsl(:,1)),1) = 0;

% If input was given as integer values between 1...255, then the output will
% equally be integers i.e., in [ 0...360 , 0...100 , 0...100 ]
if isinint
  hsl = round(hsl .* repmat([ 360 , 100 , 100 ], size(hsl, 1), 1));
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
