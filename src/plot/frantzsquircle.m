function h = frantzsquircle(s, r, varargin)
%% FRANTZSQUIRCLE
%
% Inputs:
%
%   S                       Description of argument S
% 
%   R                       Description of argument R
% 
% Outputs:
% 
%   H                       Description of argument H
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% FRANTZSQUIRCLE(S, R)
% FRANTZSQUIRCLE(HAX, S, R)
narginchk(2, 3);

% FRANTZSQUIRCLE(___)
% H = FRANTZSQUIRCLE(___)
nargoutchk(0, 1);

% Find an axes object
[cax, args, nargs] = axescheck(s, r, varargin{:});

% No axes object given, then take the current one
if isempty(cax)
  cax = gca();
end

oldhold = ishold(cax);
if ~oldhold
  hold(cax, 'on');
  coHold = onCleanup(@() hold(cax, 'off'));
end

% Extract required arguments from remaining arguments
s = args{1};
r = args{2};
args([1,2]) = [];



%% Algorithm

% Linearly spaced independent variable
t = reshape(linspace(0, 2 * pi, 361), [], 1);

% If S is close to 0, we will perform a taylor approximation of TANH
if isclose(s, 0)
  tanhs = s - s ^ 3 / 3 + 2 * s ^ 5 / 15;
  
% Non near-zero case of S
else
  tanhs = tanh(s);
  
end

% Calculate X and Y coordinates
x = r .* tanh( s .* cos(t) ) ./ tanhs;
y = r .* tanh( s .* sin(t) ) ./ tanhs;
y(isnan(y)) = 1;

% Plot
hp = plot( ...
    cax ...
  , x, y ...
);

% If figure is new, we will adjust some parameters
if ~oldhold
  cax.DataAspectRatio = [ 1 , 1 , 1 ];
  cax.XLimitMethod = 'padded';
  cax.YLimitMethod = 'padded';
  
end



%% Assign Output Quantities

% H = FRANTZSQUIRCLE(___)
if nargout > 0
  h = hp;
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
