function rgb = hsl2rgb(hsl)
%% HSL2RGB
%
% RGB = HSL2RGB(HSL)
%
% Inputs:
%
%   HSL                     Nx3 array of HSL triplets.
%
% Outputs:
%
%   RGB                     Nx3 array of RGB triplets as uint16.



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-05-30
% Changelog:
%   2023-05-30
%       * Initial release



%% Parse arguments

% HSL2RGB(HSL)
narginchk(1, 1);

% HSL2RGB(___)
% RGB = HSL2RGB(___)
nargoutchk(0, 1);



%% Algorithm

% Convert HSL values to doubles
hsl = double(hsl);

% Flag whether values are given as doubles i.e., between [0...1]
inint = any(hsl(:) > 1);

% Scale all value to be in the range of [ 0...360 , 0...100 , 0...100 ]
if inint
  hsl = hsl ./ repmat([ 1 , 100 , 100 ], size(hsl, 1), 1);
  
else
  hsl(:,1) = hsl(:,1) .* ( 360 .* ones(size(hsl, 1), 1) );
end

% Extract hue, staruation, lightness
H = hsl(:,1);
S = hsl(:,2);
L = hsl(:,3);

% Calculate color values
C = ( 1 - abs(2 .* L - 1) ) .* S;
X = C .* ( 1 - abs( mod(H / 60, 2) - 1 ) );
m = L - C ./ 2;

% Find indices where HUE is in certain parts of the color wheel
ind1 =   0 <= H & H <  60;
ind2 =  60 <= H & H < 120;
ind3 = 120 <= H & H < 180;
ind4 = 180 <= H & H < 240;
ind5 = 240 <= H & H < 300;
ind6 = 300 <= H & H < 360;

% Build RED, GREEN, and BLUE channels
r = C .* ind1 + ...
  + X .* ind2 + ....
  + 0 .* ind3 + ...
  + 0 .* ind4 + ...
  + X .* ind5 + ...
  + C .* ind6;
g = X .* ind1 + ...
  + C .* ind2 + ...
  + C .* ind3 + ...
  + X .* ind4 + ...
  + 0 .* ind5 + ...
  + 0 .* ind6;
b = 0 .* ind1 + ...
  + 0 .* ind2 + ...
  + X .* ind3 + ...
  + C .* ind4 + ...
  + C .* ind5 + ...
  + X .* ind6;

% Put RGB together and then round to two digits behind the decimal point
rgb = cat( ...
    2 ...
  , r + m ...
  , g + m ...
  , b + m ...
);

% Ensure all RGB values are in 0...1
rgb = limit(rgb, 0, 1);

% If input was integers, then the output will be in the range of [0...255]
if inint
  rgb = round(rgb .* ( 255 .* ones(size(rgb)) ));
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
