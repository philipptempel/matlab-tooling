% PLOT
%
% Files
%   anim2d                         - animates 2-dimensional data over time.
%   anim3d                         - animates 3-dimensional data over time.
%   applyplotstyle                 - Apply a plot style from `ExportSetup` to figure
%   center_cos                     - centers the coordinate system at [0, 0] i.e., moves the axes
%   circle                         - draws a circle of specified radius
%   circle3                        - draws a circle in 3D
%   cmyk2rgb                       - convert CMYK colors to RGB colors
%   colororder_pre2014b            - returns the color order of plot lines pre R2014b
%   distinguishableColors          - pick colors that are maximally perceptually distinct
%   editplotstyle                  - Edit a plot style from `ExportSetup`
%   enlargelimits                  - Automatically enlarge limits of the curent axis
%   fbaspect                       - Set figure box aspect ratio
%   figcheck                       - Process figure objects from input list
%   figplot                        - opens a figure and plots inside this figure.
%   gpf                            - Get the given handles parent figure
%   hex2rgb                        - 
%   hsl2rgb                        - HSL2RGB
%   isallaxes                      - Checks whether the given handle is purely axes or not
%   isfig                          - checks whether the given handle is a figure handle or not
%   isplot2d                       - Check the given axes against being a 2D plot i.e., created by plot()
%   isplot3d                       - Check the given axes against being a 3D plot i.e., created by plot3()
%   lame                           - plots the LAMÉ superellipses
%   lsplotstyles                   - LSPLOTSTYLES
%   max_fig                        - maximizes the current or given figure
%   maxplotvalue                   - Determine the maximum plotted value along all axes
%   minplotvalue                   - Determine the minimum plotted value along all axes
%   plot_addPointPlaneIntersection - Adds intersection indicator for a point on the
%   plot_colbox                    - plots a colored box (patch) for the given color
%   plot_coordaxes                 - Add a frame of reference to the current plot
%   plot_cyclelinestyles           - adds different line styles to each plot in an axis
%   plot_houghlines                - plot lines from houghlines
%   plot_markers                   - Plot some markers on the lines given in Axes
%   plot_zoom                      - plots a zoom region into the given axes
%   plotrange                      - Determine the range of plotted data from min to max
%   point                          - Define the input parser
%   publishPlotStyles              - Publish plot styles to MATLAB's `ExportSetup` directory
%   reusefigure                    - Reuse a figure or create new one
%   rgb2cmyk                       - convert RGB colors to CMYK colors
%   rgb2hex                        - 
%   rgb2hsl                        - RGB2HSL
%   rgbblend                       - Blend two RGB triplets
%   rgbdarker                      - 
%   rgblighter                     - RGBLIGHTER
%   ruler                          - plots a vertical or horizontal ruler at the given position
%   semilogxy                      - plots a 3D plot with logarithmic X- and Y-axis
%   setfigureratio                 - sets the ratio of the current or given figure
%   shadow3                        - plots a 3D shadow3 plot of all data into the given axes onto the given
%   spans                          - Obtain data span of a given ruler
%   sweep3                         - SWEEP3
%   textinterpreters               - Set default text interpreters on graphics object
%   tightfig                       - tightfig: Alters a figure so that it has the minimum size necessary to
%   usdistcolors                   - creates distinguishable colors complying with University of
%   uslayout                       - applies the corresponding figure to University of Stuttgarts
%   uslinestyles                   - creates a list of N unique lineseries plot styles
%   viewbird                       - VIEWBIRD
%   viewbottom                     - VIEWBOTTOM
%   viewdimetric                   - VIEWDIMETRIC
%   viewfront                      - VIEWFRONT
%   viewiso                        - VIEWISO
%   viewleft                       - VIEWLEFT
%   viewrear                       - VIEWREAR
%   viewreset                      - VIEWRESET
%   viewright                      - VIEWRIGHT
%   viewtop                        - VIEWTOP
%   xspan                          - Return the current axes data span over the X-Axis
%   yspan                          - Return the current axes data span over the Y-Axis
%   zoomax                         - in or out into an axes depending on the scaling factor
%   zspan                          - Return the current axes data span over the Z-Axis
