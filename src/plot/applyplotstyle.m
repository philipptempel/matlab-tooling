function applyplotstyle(styletype, styleflavor)
%% APPLYPLOTSTYLE Apply a plot style from `ExportSetup` to figure
%
% APPLYPLOTSTYLE(STYLETYPE, STYLEFLAVOR) applies user-defined plot style type
% STYLETYPE with plot flavor STYLEFLAVOR found in `PREFDIR(0, 'ExportSetup')` to
% the current figure.
%
% APPLYPLOTSTYLE(HF, ...) applies the plot style to the given figure HF.
%
% Inputs:
%
%   STYLETYPE               Type of plot style to apply.
%
%   STYLEFLAVOR             Flavor of plot style to apply.
%
%   HF                      Single figure handle to apply style to.
%
% See also:
%   HGEXPORT
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% APPLYPLOTSTYLE(STYLETYPE)
% APPLYPLOTSTYLE(STYLETYPE, STYLEFLAVOR)
% APPLYPLOTSTYLE(HF, ___)
narginchk(1, 3);

% APPLYPLOTSTYLE(___)
nargoutchk(0, 0);

% Split all arguments into figure and other arguments
[hf, args] = figcheck(styletype, styleflavor);

% No figure? Default to current figure
if isempty(hf)
  hf = gcf();
end



%% Algorithm

% Read style configuration
st = hgexport('readstyle', strjoin(cat(2, args{:}), filesep()));

% Convert width and height from export style char to numeric values
stwh = [ str2double(st.Width) , str2double(st.Height) ];
dpi  = str2double(st.Resolution);
stu  = st.Units;

% If target units is POINTS, we will convert them to CM
if strcmp(stu, 'points')
  % If both target width and target height are NaN, we will get the figure's
  % current width and height in the target units
  if isnan(stwh(1)) && isnan(stwh(2))
    stwh = getwhin(hf, stu);
  
  % Width is "AUTO", height not
  elseif isnan(stwh(1)) && ~isnan(stwh(2))
    [stwh(1), ~] = getwhin(hf, stu);
    
  % Height is "AUTO", width not
  elseif ~isnan(stwh(1)) && isnan(stwh(2))
    [~, stwh(2)] = getwhin(hf, stu);
    
  end
  
  % Convert width and height from POINTS to CENTIMETER
  stwh = pts2cm(stwh, dpi);
  
% If target units is INCHES, we will convert them to CM
elseif strcmp(stu, 'inches')
  % If both target width and target height are NaN, we will get the figure's
  % current width and height in the target units
  if isnan(stwh(1)) && isnan(stwh(2))
    stwh = getwhin(hf, stu);
  
  % Width is "AUTO", height not
  elseif isnan(stwh(1)) && ~isnan(stwh(2))
    [stwh(1), ~] = getwhin(hf, stu);
    
  % Height is "AUTO", width not
  elseif ~isnan(stwh(1)) && isnan(stwh(2))
    [~, stwh(2)] = getwhin(hf, stu);
    
  end
  
  % Convert width and height from INCHES to CENTIMETERS
  stwh = in2cm(stwh);
  
else
  % If both target width and target height are NaN, we will get the figure's
  % current width and height in the target units
  if isnan(stwh(1)) && isnan(stwh(2))
    stwh = getwhin(hf, stu);
  
  % Width is "AUTO", height not
  elseif isnan(stwh(1)) && ~isnan(stwh(2))
    [stwh(1), ~] = getwhin(hf, stu);
    
  % Height is "AUTO", width not
  elseif ~isnan(stwh(1)) && isnan(stwh(2))
    [~, stwh(2)] = getwhin(hf, stu);
    
  end
  
end

% Overwrite width, height, and units of style to a fixed set
st.Width  = stwh(1);
st.Height = stwh(2);
st.Units  = 'centimeters';

% Apply style
hgexport( ...
    hf ...
  , tempname() ...
  , st ...
  , 'ApplyStyle', true ...
);

% Set print paper size from the style and from above calculated values
hf.PaperPositionMode    = 'manual';
hf.PaperUnits           = st.Units;
hf.PaperSize            = stwh;
hf.PaperPosition([1,2]) = [ 0 , 0 ];
hf.PaperPosition([3,4]) = hf.PaperSize;

% Store resolution into figure's UserData
if isempty(hf.UserData)
  hf.UserData = struct();
end
hf.UserData.Resolution = dpi;


end


function varargout = getwhin(h, u)
%% GETWHIN
%
% WH = GETWHIN(H, UNITS)
%
% [W, H] = GETWHIN(H, UNITS)



% GETWHIN(H, UNITS)
% GETWHIN(H, UNITS)
narginchk(2, 2);

% WH = GETWHIN(___)
% [W, H] = GETWHIN(___)
nargoutchk(1, 2);


oldu = h.Units;
h.Units = u;
wh = h.Position([3,4]);
h.Units = oldu;

if nargout > 1
  varargout = num2cell(wh, 1);
else
  varargout = { wh };
end


end


function cm = pts2cm(pts, dpi)
%% PTS2CM



cm = in2cm(pts2in(pts, dpi));


end


function in = pts2in(pts, dpi)
%% PTS2IN



in = pts ./ dpi;


end


function cm = in2cm(in)
%% IN2CM



cm = in .* 2.54;


end


function pts = cm2pts(cm, dpi)
%% CM2PTS



pts = in2pts(cm ./ 2.54, dpi);


end


function pts = in2pts(in, dpi)
%% IN2PTS



pts = in .* dpi;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
