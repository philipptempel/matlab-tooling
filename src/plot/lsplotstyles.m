function lsplotstyles()
%% LSPLOTSTYLES



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-05-12
% Changelog:
%   2023-05-12
%       * Initial release



%% Parse arguments



%% Algorithm

% Directory where plot styles are stored
esdir = fullfile(prefdir(0), 'ExportSetup');

% Find all files
esfiles = dir(fullfile(esdir, '*.txt'));

% Clean filename i.e., remove file extension 
ps = strnatsort(arrayfun(@(f) erase(f.name, '.txt'), esfiles, 'UniformOutput', false));

% Print everything to screen
for ips = 1:numel(ps)
  fprintf('   %s\n', ps{ips});
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
