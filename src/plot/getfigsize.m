function varargout = getfigsize(varargin)
%% GETFIGSIZE Get figure width and height in a given unit
%
% FWH = GETFIGSIZE() gets the current figure's size FWH in the figure's current
% unit.
% 
% FWH = GETFIGSIZE(UNIT) gets the current figure's width and height FWH in units
% of UNIT.
%
% [FW, FH] = GETFIGSIZE(___) gets width and height separately.
%
% Inputs:
%
%   UNIT                    Char array of unit to get current figure's width and
%                           height in.
%
% Outputs:
%
%   FWH                     1x2 array of figure's width and height as [ W , H ]/
%
%   FH                      Scalar figure height.
%
%   FW                      Scalar figure width.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% GETFIGSIZE(UNIT)
% GETFIGSIZE(FIG, UNIT)
narginchk(0, 2);

% GETFIGSIZE(___)
% FWH = GETFIGSIZE(___)
% [FW, FH] = GETFIGSIZE(___)
nargoutchk(0, 2);

% Default arguments
hf = [];
units = [];

% Get arguments
args = varargin;
nargs = numel(args);

% Check if first argument is a figure object
if nargs > 0 && ...
        ( ( isnumeric(args{1}) && isscalar(args{1}) && isgraphics(args{1}, 'Figure') ) ...
          || isa(args{1},'matlab.ui.Figure') ...
        )
  hf    = handle(args{1});
  args  = args(2:end);
  nargs = nargs - 1;
  
end

% Have arguments left?
if nargs > 0
  units = args{1};
  args(1) = [];
  nargs = nargs - 1;
end

% If no figure found yet, get the current one
if isempty(hf)
  hf = gcf();
end


%% Algorithm

% User had requested a specific unit, so configure figure to use the new units
if ~isempty(units)
  % Get current units
  hfu = get(hf, {'Units'});
  % Set unit to the desired units
  set(hf, 'Units', units);
  
end

% Get position
fp = cell2mat(get(hf, {'Position'}));
% Extract only width and height from positions
fwh = fp(:,[3,4]);

% User had requested a specific unit, so restore original unit
if ~isempty(units)
  % And restore original units
  set(hf, {'Units'}, hfu);
  
end



%% Assign Output Quantities

% [FW, FH] = GETFIGSIZE(___)
if nargout > 1
  varargout = num2cell(fwh, 1);

% GETFIGSIZE(___)
% FWH = GETFIGSIZE(___)
else
  varargout = { fwh };
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
