function c = struct2nvpairs(s)%#codegen
%% STRUCT2NVPAIRS Turn a structure into cell array of Name/Value pairs
%
% C = STRUCT2NVPAIRS(S) converts structure S to cell array C like MATLAB's built-in
% STRUCT2CELL, but it retains the fieldnames of S.
%
% Inputs:
%
%   S                       Structure to convert.
%
% Outputs:
%
%   C                       1x2N cell array.
%
% See also:
%   FIELDNAMES ONOFFSTATE



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2022-03-04
% Changelog:
%   2022-03-04
%       * Initial release



%% Parse arguments

% STRUCT2NVPAIRS(S)
narginchk(1, 1);

% C = STRUCT2NVPAIRS(___)
nargoutchk(0, 1);



%% Algorithm

% Simple one-liner
c = reshape(transpose(cat(2, fieldnames(s), struct2cell(s))), 1, []);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
