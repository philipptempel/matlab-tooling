function cm = in2cm(in)
%% IN2CM Convert inches to centimeters
%
% CM = IN2CM(IN) converts inches IN to centimeters CM
%
% Inputs:
%
%   IN                      Array of values in inches.
%
% Outputs:
%
%   CM                      Array of values in centimeters.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% IN2CM(IN)
narginchk(1, 1);

% CM = IN2CM(___)
nargoutchk(0, 1);



%% Algorithm

cm = in .* 2.54;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
