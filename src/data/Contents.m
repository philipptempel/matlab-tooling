% DATA
%
% Files
%   allcomb               - - All combinations
%   bin2str               - Convert binary string representation to its string/char form
%   bool2str              - Turn a boolean value into its string form
%   comparestructs        - Compare two structures
%   cycliccell            - repeats a cell as a cycle
%   eltimstat             - Calculate statistics of elapsed times
%   first                 - gets the first element of the given argument
%   last                  - gets the last element of the given argument
%   limit                 - the given value between minimum and maximum
%   mat2tex               - converts a matrix to LaTeX compatible tabular syntax
%   mergestructs          - merges multiple structs into one
%   parallel_axis_inertia - determines the inertia of another point O following the
%   str2bin               - Convert a string to its binary representation
%   struct2array          - Convert a structure to an array i.e., get all values from structure
%   struct2nvpairs        - Turn a structure into cell array of Name/Value pairs
%   struct2vars           - Convert a structure to local variables
%   structget             - Get field's value from a structure of a default value
%   structtime2ts         - Turn a simulink sink "struct with time" to a timeseries data
%   swap                  - Flip two variables
%   textable              - is a MATLAB table object that supports export to TeX files
%   unmatchedcell         - turns the given structure of unmatched IP parameters to a cell
%   uuid                  - Create a UUID
