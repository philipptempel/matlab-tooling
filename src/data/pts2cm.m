function cm = pts2cm(pts, dpi)
%% PTS2CM Convert points to centimeters
%
% CM = PTS2CM(PTS, DPI) converts number of points PTS with resolution DPI to
% centimeters CM.
%
% Inputs:
%
%   PTS                     Array of number of points.
% 
%   DPI                     Array of resolution (dots per inch).
%
% Outputs:
%
%   CM                      Array of values in centimeters.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% PTS2CM(PTS, DPI)
narginchk(2, 2);

% CM = PTS2CM(___)
nargoutchk(0, 1);



%% Algorithm

cm = in2cm(pts2in(pts, dpi));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
