function v = optsget(options, name, default, flag)%#codegen
%% OPTSGET Get an options value from an options structure
%
% V = OPTSGET(OPTIONS, NAME, DEFAULT)
%
% Inputs:
%
%   OPTIONS                 Description of argument OPTIONS
% 
%   NAME                    Description of argument NAME
% 
%   DEFAULT                 Description of argument DEFAULT
% 
%   FLAG                    Description of argument FLAG
%
% Outputs:
%
%   V                       Description of argument V



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2022-02-28
% Changelog:
%   2022-02-28
%       * Initial release



%% Parse arguments

% OPTSGET(OPTIONS, NAME, DEFAULT)
% OPTSGET(OPTIONS, NAME, DEFAULT, FLAG)
narginchk(3, 4);

% V = OPTSGET(___)
nargoutchk(0, 1);

% Undocumented way of quickly getting a value from the options structure
if nargin < 4 || isempty(flag)
  flag = [];
end



%% Algorithm

v = structget(options, name, default, flag);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
