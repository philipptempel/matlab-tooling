function pts = in2pts(in, dpi)
%% IN2PTS Convert inches to points
%
% PTS = IN2PTS(IN, DPI) converts inches IN to number of points PTS with
% resolution DPI.
%
% Inputs:
%
%   IN                      Array of values in inches.
% 
%   DPI                     Array of resolution (dots per inch).
%
% Outputs:
%
%   PTS                     Array of number of points.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% IN2PTS(IN, DPI)
narginchk(2, 2);

% PTS = IN2PTS(___)
nargoutchk(0, 1);



%% Algorithm

pts = in .* dpi;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
