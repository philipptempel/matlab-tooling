function v = structget(strct, name, default, flag)%#codegen
%% STRUCTGET Get field's value from a structure or a default value
%
% V = STRUCTGET(STRUCTURE, NAME, DEFAULT)
%
% V = STRUCTGET(STRUCTURE, NAME, DEFAULT, FLAG)
%
% Inputs:
%
%   STRUCTURE               Structure from which to get value.
% 
%   NAME                    Field name to retrieve.
% 
%   DEFAULT                 Default value to return if field NAME does not exist
%                           in STRUCTURE.
% 
%   FLAG                    Flag whether to get value fast or not. Options are
%                             []      Normal retrieval
%                             'fast'  Fast retrieval without error checking
%                           Default: []
%
% Outputs:
%
%   V                       Value found for STRUCTURE.(NAME) or DEFAULT if field
%                           does not exist.



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-05-26



%% Parse arguments

% STRUCTGET(STRUCTURE, NAME, DEFAULT)
% STRUCTGET(STRUCTURE, NAME, DEFAULT, FLAG)
narginchk(3, 4);

% V = STRUCTGET(___)
nargoutchk(0, 1);

% Undocumented way of quickly getting a value from the structure
if nargin > 3 && isequal(char(flag), 'fast')
  v = structgetfast(strct, name, default);
  
  return
  
end



%% Algorithm

% No options given?
if isempty(strct)
  % Default value returned
  v = default;
  
  return;
  
end

% Get allowed names and requested name in lower case
names     = fieldnames(strct);
names_low = lower(names);
name_low  = lower(name);

j = find(startsWith(names_low, name_low));

% If no matches
if isempty(j)
  v = default;
  
  return

% Or more than one match
elseif numel(j) > 1
  % Check for any exact matches (in case any names are subsets of others)
  k = find(strcmp(name_low, names));
  
  if numel(k) == 1
    j = k;
    
  else
    
    matches = strjoin(names(j), ', ');
    
    error(message('MATLAB:odeget:AmbiguousPropName', name, matches));
    
  end
  
end

% Get the value
v = strct.(names{j});

% Last but not least, check if the value found is empty, in which case we will
% return the default
if isempty(v)
  v = default;
  
end


end


function v = structgetfast(options, name, default)
%% STRUCTGETFAST



if isfield(options, name) && ~isempty(options.(name))
  v = options.(name);
  
else
  v = default;
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
