function c = uplus(a)%#codegen
%% UPLUS Unary plus of a structure
%
% C = +(A) returns structure A and stores it in C.
%
% C = UPLUS(A);
%
% Inputs:
%
%   A                       Description of argument A
% 
% Outputs:
%
%   C                       Description of argument C



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% UPLUS(A)
narginchk(1, 1);

% UPLUS(___)
% C = UPLUS(___)
nargoutchk(0, 1);



%% Algorithm

% Simply copy over
c = a;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
