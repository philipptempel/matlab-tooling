function v = objget(obj, name, default, flag)%#codegen
%% OBJGET Get property's value from an object or a default value
%
% V = OBJGET(OBJECT, NAME, DEFAULT)
%
% V = OBJGET(OBJECT, NAME, DEFAULT, FLAg)
%
% Inputs:
%
%   OBJECT                  Object from which to get value.
% 
%   NAME                    Field name to retrieve.
% 
%   DEFAULT                 Default value to return if field NAME does not exist
%                           in OBJECT.
% 
%   FLAG                    Flag whether to get value fast or not. Options are
%                             []      Normal retrieval
%                             'fast'  Fast retrieval without error checking
%                           Default: []
%
% Outputs:
%
%   V                       Value found for OBJECT.(NAME) or DEFAULT if field
%                           does not exist.



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-05-26



%% Parse arguments

% OBJGET(OBJECT, NAME, DEFAULT)
% OBJGET(OBJECT, NAME, DEFAULT, FLAG)
narginchk(3, 4);

% V = OBJGET(___)
nargoutchk(0, 1);

% Undocumented way of quickly getting a value from the object
if nargin > 3 && isequal(char(flag), 'fast')
  v = objgetfast(obj, name, default);
  
  return
  
end



%% Algorithm

% No options given?
if isempty(obj)
  % Default value returned
  v = default;
  
  return;
  
end

% Get allowed names and requested name in lower case
names     = properties(obj);
names_low = lower(names);
name_low  = lower(name);

j = find(startsWith(names_low, name_low));

% If no matches
if isempty(j)
  v = default;
  
  return

% Or more than one match
elseif numel(j) > 1
  % Check for any exact matches (in case any names are subsets of others)
  k = find(strcmp(name_low, names));
  
  if numel(k) == 1
    j = k;
    
  else
    
    matches = strjoin(names(j), ', ');
    
    error(message('MATLAB:odeget:AmbiguousPropName', name, matches));
    
  end
  
end

% Empty initial value
v = [];

% Get the value
if isprop(obj, names{j})
  v = obj.(names{j});

end

% Last but not least, check if the value found is empty, in which case we will
% return the default
if isempty(v)
  v = default;
end


end


function v = objgetfast(obj, name, default)
%% OBJGETFAST



if isprop(obj, name) && ~isempty(obj.(name))
  v = obj.(name);
  
else
  v = default;
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
