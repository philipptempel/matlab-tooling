function s = bin2str(b)%#codegen
%% BIN2STR Convert binary string representation to its string/char form
%
% STR = BIN2STR(BIN) converts binary value BIN into its string representation
% STR.
%
% Inputs:
%
%   BIN                     Vector of binary values to convert to string.
%
% Outputs:
%
%   STR                     String representation of binary value BIN.



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-03-03
% Changelog:
%   2023-03-03
%       * Initial release



%% Parse arguments

% BIN2STR(B)
narginchk(1, 1);

% BIN2STR(___)
% S = BIN2STR(___)
nargoutchk(0, 1);



%% Algorithm

b_  = transpose(reshape(b, [8 , length(b) / 8 ] ));
if isa(b_, 'double')
  s = char(transpose(bin2dec(char(b_ + 48))));
else
  s = char(transpose(bin2dec(b_)));
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
