function in = cm2in(cm)
%% CM2IN Convert centimeters to inches
%
% IN = CM2IN(CM) converts centimeters CM to inches IN.
%
% Inputs:
%
%   CM                      Array of values in centimeters.
%
% Outputs:
%
%   IN                      Array of values in inches.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% CM2IN(CM)
narginchk(1, 1);

% IN = CM2IN(___)
nargoutchk(0, 1);



%% Algorithm

in = cm ./ 2.54;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
