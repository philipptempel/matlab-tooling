function b = str2bin(s)%#codegen
%% STR2BIN Convert a string to its binary representation
%
% BIN = STR2BIN(STR) converts string STR to its binary representation B.
%
% Inputs:
%
%   STR                     String to convert to binary.
%
% Outputs:
%
%   BIN                     Binary representation of string STR.



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-03-03
% Changelog:
%   2023-03-03
%       * Initial release



%% Parse arguments

% STR2BIN(TEXT)
narginchk(1, 1);

% STR2BIN(___)
% B = STR2BIN(___)
nargoutchk(0, 1);



%% Algorithm

b = transpose(reshape(transpose(dec2bin(s, 8)), [], 1)) - 48;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
