function s = nvpairs2struct(varargin)%#codegen
%% NVPAIRS2STRUCT Turn a Name/Value pair cell array into a structure
%
% S = NVPAIRS2STRUCT(Name, Value, ...) converts Name/Value pairs cell into a
% structure.
%
% Outputs:
%
%   S                       Structure
%
% See also:
%   FIELDNAMES



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% NVPAIRS2STRUCT(___)
narginchk(0, Inf);

% S = NVPAIRS2STRUCT(___)
nargoutchk(0, 1);



%% Algorithm

% If any arguments
if nargin > 1
  % Simple one-liner
  s = cell2struct(varargin(2:2:end), varargin(1:2:end), 2);
  
% No arguments
else
  s = struct();
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
