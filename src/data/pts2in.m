function in = pts2in(pts, dpi)
%% PTS2IN Convert points to inches
%
% IN = PTS2IN(PTS, DPI) converts of points PTS with resolution DPI to inches IN.
%
% Inputs:
%
%   PTS                     Array of number of points.
% 
%   DPI                     Array of resolution (dots per inch).
%
% Outputs:
%
%   IN                      Array of values in inches.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% PTS2IN(PTS, DPI)
narginchk(2, 2);

% IN = PTS2IN(___)
nargoutchk(0, 1);



%% Algorithm

in = pts ./ dpi;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
