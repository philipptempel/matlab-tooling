function pts = cm2pts(cm, dpi)
%% CM2PTS Convert centimeters to points
%
% PTS = CM2PTS(CM, DPI) converts centimeters CM to number of points PTS with
% resolution DPI.
%
% Inputs:
%
%   CM                      Array of values in centimeters.
% 
%   DPI                     Array of resolution (dots per inch).
%
% Outputs:
%
%   PTS                     Array of number of points.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% CM2PTS(CM, DPI)
narginchk(2, 2);

% PTS = CM2PTS(___)
nargoutchk(0, 1);



%% Algorithm

pts = in2pts(cm2in(cm), dpi);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
