function c = getundoc(arg)
%% GETUNDOC Get Undocumented Object Properties
%
% GETUNDOC('OBJECT') or GETUNDOC(H) returns a structure of undocumented
% properties (names & values) for the object having handle H or indentified by
% the string 'OBJECT'.
%
% For example, GETUNDOC('axes') or GETUNDOC(gca()) returns undocumented property
% names and values for the axes object.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%   Yair Altman <altmany@gmail.com>
%   D.C. Hanselman <MasteringMatlab@yahoo.com>



%% Parse arguments

% GETUNDOC(OBJ)
narginchk(1, 1);



%% Algorithm

if ischar(arg) % GETUNDOC('OBJECT')
  switch lower(arg)
    case 'root'
      h = 0;
      hf = 0;
    
    % figure
    case 'figure'
      h = figure('Visible', 'off');
      hf = h;
    
    % some other string name of an object
    otherwise
      hf = figure('Visible', 'off');
      object = str2func(arg);
    
      try
        h = object('Parent', hf, 'Visible', 'off');
      
      catch
        error('Unknown Object Type String Provided.')
      
      end

  end
  
% GETUNDOC(H)
elseif ishandle(arg)
  h = arg;
  hf = 0;
 
else
  error('Unknown Object Handle Provided.')
 
end

wstate = warning();
% supress warnings about obsolete properties
warning('off');

% Fails in HG2
try
  set(0, 'HideUndocumented', 'off');
catch
end

% get props including undocumented
undocfnames = fieldnames(get(h));
  
% Fails in HG2
try
  set(0, 'HideUndocumented', 'on');
catch
end

% get props excluding undocumented
docfnames = fieldnames(get(h));

% Yair 18/3/2010 - add a few more undocs:
try
  % This works in HG1
  props = get(classhandle(handle(h)), 'Properties');
  undocfnames = [ undocfnames ; get(props(strcmp(get(props, 'Visible'), 'off')), 'Name') ];
  
catch
  % Yair 18/9/2011: In HG2, the above fails, so use the following workaround:
  try
    prop = findprop(handle(h), undocfnames{1});
    props = prop.DefiningClass.PropertyList;
    undocfnames = [ undocfnames ; transpose({ props.Name }) ];
    
  catch
    % ignore...
  end
  
end

% extract undocumented
c = setdiff(undocfnames, docfnames);

% Get the values in struct format, if relevant
if ~isempty(c)
  s = struct;
  for fieldIdx = 1:length(c)
    try
      fieldName = c{fieldIdx};
      s.(fieldName) = get(h, fieldName);
      
    catch
      s.(fieldName) = '???';
      
    end
    
  end
  
  c = s;

end
% Yair end

% delete hidden figure holding selected object
if hf ~= 0
  delete(hf)
end

warning(wstate);

end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
