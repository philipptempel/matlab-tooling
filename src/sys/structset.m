function ns = structset(s, varargin)%#codegen
%% STRUCTSET Set or update an options structure's Name/Value pairs
%
% NS = STRUCTSET(S, Name, Value, ...) sets Name/Value pairs onto structure.
%
% NS = STRUCTSET(S, NEWOPTS) overrides structure OLDOPTS with new
% options from NEWOPTS structure.
%
% NS = STRUCTSET(S, Name, Value) updates old structure OLDOPTS with
% new Name/Value pairs.
%
% Inputs:
%
%   S                       A structure to use as base for updating its
%                           Name/Value pairs.
%
%   NEWOPTS                 An structure with new name/value pairs.
%
% Outputs:
%
%   NS                      New structure with updated values.



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% STRUCTSET()
% STRUCTSET(NAME, VALUE, ...)
% STRUCTSET(OLDOPTS, NEWOPTS)
% STRUCTSET(OLDOPTS, Name, Value, ...)
narginchk(0, Inf);

% STRUCTSET(___)
% NS = STRUCTSET(___)
nargoutchk(0, 1);



%% Algorithm

% Create other structure based on type of varargin
if nargin > 1 && ~isstruct(varargin{1})
  other = struct(varargin{:});
  
else
  other = varargin{1};
  
end

% Now merge two structures by letting the latter override the former's fields
ns = s + other;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
