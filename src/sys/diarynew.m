function diarynew()%#codegen
%% DIARYNEW Make a new diary file



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2021-04-30
% Changelog:
%   2021-04-30
%       * Initial release



%% Parse arguments

% DIARYNEW()
narginchk(0, 0);

% DIARYNEW(___)
nargoutchk(0, 0);



%% Algorithm

% Name of log file
lname = fullfile(userpath(), 'diaries', 'commands.log');

% Create path to new diary file directory
dfolder = fileparts(lname);
if 7 ~= exist(dfolder, 'dir')
  mkdir(dfolder)
end

% Set diary
diary(lname);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
