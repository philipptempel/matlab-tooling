function filename = safefilename(varargin)
%% SAFEFILENAME Provides a safe filename for a given file
%
% SAFEFILENAME(FOLDERNAME1, FOLDERNAME2, ..., FILENAME) provides a safe filename
% for the given file by backing any existing file up (suffixed with its
% content-modified timestamp).
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% SAFEFILENAME(FILENAME)
narginchk(1, Inf);

% SAFEFILENAME(___)
% FILENAME = SAFEFILENAME(___)
nargoutchk(0, 1);



%% Algorithm

% Extract information on where to save data to
[fpath, fname, fext] = fileparts(fullfile(varargin{:}));

% No path?
if isempty(fpath)
  % Default behavior is to save in the current working directory
  fpath = pwd();
end
% No file extension?
if isempty(fext)
  % Default is a MAT file
  fext = '.mat';
end

% Filename under which to save data
filename = fullfile(fpath, [ fname , fext ]);

% If target file exists...
if 2 == exist(filename, 'file')
  % Create a backup of the file by suffixing it with its last modification date
  movefile( ...
      filename ...
    , fullfile( ...
        fpath ...
      , [ ...
        fname ...
        , '_' ...
        ... % Prepend the CONTENT MODIFIED time stamp to the old file
        , char( ...
            getmtime( ...
                filename ...
              , 'TimeZone', 'UTC' ...
              , 'Format'  , 'yyyy-MM-dd''T''HHmmssXX' ...
            ) ...
          ) ...
        , fext ...
      ] ...
    ) ...
  );
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
