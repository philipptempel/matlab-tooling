% SYS
%
% Files
%   alldirs                     - Finds all files in directory DIR and returns them in a structure
%   allfiles                    - Finds all files in directory DIR and returns them in a structure
%   argstring                   - truns a list of arguments into a human-readable string
%   build                       - Build mexw functions for some m-files
%   bytes2str                   - turns the number of bytes into a human readable string
%   classdiagram                - Create a class hierarchy diagram for a given package
%   cmdwinsize                  - Return the current command window size
%   color                       - Create a color RGB triplet from a human readable name
%   computername                - returns the name of the computer in the local network
%   cprintf                     - displays styled formatted text in the Command Window
%   create_contents             - creates the CONTENTS.M files for this project
%   create_docs                 - creates the docs for project MATLAB-Tooling from each functions'
%   datestr8601                 - Convert a Date Vector or Serial Date Number to an ISO 8601
%   deletealltimers             - deletes all timers whether they are visible or not
%   diaryfile                   - Get the global diary file
%   diarynew                    - Make a new diary file
%   diaryrotate                 - Rotate diary files
%   dispstat                    - Prints overwritable message to the command line. If you dont want to keep
%   ensuredir                   - Ensure a certain directory exists
%   focus_cmdwin                - gives focus to the command window
%   focus_editor                - gives programmatic focus to the editor
%   funcstring                  - Yield string representation of a function
%   geth1                       - Get H1 line of a file
%   git                         - GIT
%   humansize                   - Default decimals
%   iif                         - Allows conditionals in inline and anonymous functions
%   isinpath                    - Checks whether the given path is part of MATLAB's environment path
%   KWArgs                      - Is a python kwargs-like object
%   makecontents                - Make a Contents.M file in the given directory
%   matlab_logo                 - 
%   mlproject                   - Locate the MATLAB project directory in the parent directories.
%   mlsettings                  - allows to save and restore MATLAB settings
%   normalizepath               - Normalizes a set of paths
%   objectcheck                 - checks for an object of a calling class in the list of arguments
%   optsget                     - Get an options value from an options structure
%   optsset                     - Set or update an options structure's Name/Value pairs
%   pack_files_and_dependencies - packs dependent files into one folder
%   pbcopy                      - A unix-command like wrapper around CLIPBOARD
%   pbpaste                     - A unix-command like wrapper around CLIPBOARD
%   progressbar                 - creates a nicer progress bar window for progress information
%   pvparams                    - Split variable arguments into numeric and name/value pairs
%   restart                     - executes a few functions to reset MATLAB workspace
%   rgb                         - converts a conventional RGB representation into MATLAB RGB format
%   safesave                    - Like save, just safer
%   save_figure                 - Saves the figure under the specified filename with variable types
%   save_ws                     - saves the whole workspace to a unique filename 
%   seconds2human               - SECONDS2HUMAN( seconds )   Converts the given number of seconds into a
%   secs2hms                    - Convert seconds to a human readable time format
%   setrng                      - sets the random number generator to a set or pre-defined options
%   shadowed                    - Return path to a shadowed MATLAB functon
%   siprefix                    - Returns the SI prefix for V.
%   stopalltimers               - stops all timers whether they are visible or not
%   structset                   - Set or update an options structure's Name/Value pairs
%   test                        - Run tests for several packages
%   time2str                    - turns a duration in seconds into a human readable string
%   tsave                       - 
%   userhome                    - Get path to current user's HOME directory
%   username                    - returns the current user's username
%   varsize                     - determines the size of each variable given
%   wd                          - MATLAB implementation of `warpdirectory`
