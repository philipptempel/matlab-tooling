function varargout = varsize(varargin)
%% VARSIZE determines the size of each variable given
%
% VARSIZE() prints the size of all of caller's variables in bytes.
%
% VARSIZE(X) prints the size of variable X on the screen.
%
% VARSIZE(X, Y) prints the size of variables X and Y on the screen.
%
% S = VARSIZE(...) returns the size of all variables in bytes in struct S.



%% File information
% Author: Philipp Tempel <matlab@philipptempel.me>
% Date: 2022-02-28
% Changelog:
%   2022-02-28
%       * Beautify output of function
%       * Add support for 0-argument call
%       * Code improvement
%       * Update H1 documentation
%   2021-12-14
%       * Update email address of Philipp Tempel
%   2018-02-06
%       * Initial release



%% Validate arguments

% VARSIZE()
% VARSIZE(X, ...)
narginchk(0, Inf);

% VARSIZE(___)
% S = VARSIZE(___)
nargoutchk(0, 1);



%% Algorithm

% VARSIZE(VAR1, VAR2, ...)
if nargin > 0
  vars = repmat(struct( ...
      'name'      , [] ...
    , 'size'      , [] ...
    , 'bytes'     , [] ...
    , 'class'     , [] ...
    , 'global'    , [] ...
    , 'sparse'    , [] ...
    , 'complex'   , [] ...
    , 'nesting'   , [] ...
    , 'persistent', [] ...
  ), nargin, 1);
  
  % Loop over all arguments
  for iarg = 1:nargin
    varname = inputname(iarg);
    if isempty(varname)
      varname = sprintf('in%.0f', iarg);
    end
    var_ = varargin{iarg};
    vars(iarg) = whos('var_');
    vars(iarg).name = varname;
  end
  
% VARSIZE
else
  vars = evalin('caller', 'whos();');
  
end

% Convert bytes to respective byte sizes
vbytes = bytes2str([ vars.bytes ]);
if numel(vars) == 1
  vbytes = { vbytes };
end
[ vars.sbytes ] = deal(vbytes{:});



%% Assign output quantities
% No output, display the data
if nargout == 0
  % Extract variable names and sizes
  names = { vars.name };
  bytes = { vars.sbytes };
  
  % Calculate maximum column width over all columns
  col_widths = [ max([ 12 , 2 + cellfun(@length, names) ]) , max([ 12 , 2 + cellfun(@length, bytes) ]) ];
  
  fprintf('\n');
  fprintf('    ');
  fprintf('%-*s', [ col_widths(1) , 'Name' ]);
  fprintf('%-*s', [ col_widths(2) , 'Bytes' ]);
  fprintf('\n');
  fprintf('\n');
  
  % Loop over all variables
  for iname = 1:numel(names)
    fprintf('    ');
    fprintf('%-*s', [ col_widths(1) , names{iname} ]);
    fprintf('%*s', [ col_widths(2) , strrep(bytes{iname}, ' b', ' b ') ]);
    fprintf('\n');
  end
  
  fprintf('\n');
    
end

% One output: return the structure
if nargout > 0
  varargout{1} = vars;
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changes" section of the header.
