function p = userhome()
%% USERHOME Get path to current user's HOME directory
%
% P = USERHOME() returns the absolute path to the user's home directory and
% works on both PC (Windows) and UNIX (Linux, mac) systems.
%
% Outputs:
%
%   P                       Absolute path to the user's home directory.



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-04-19
% Changelog:
%   2023-04-19
%       * Initial release



%% Parse arguments

% USERHOME()
narginchk(0, 0);

% USERHOME(___)
% P = USERHOME(___)
nargoutchk(0, 1);



%% Algorithm

if ispc()
  p = getenv('USERPROFILE');
  
else
  p = getenv('HOME');
  
end

% Ensure the path does not end with a file separator (direcotry slash)
p = strip(p, 'right', filesep());


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
