function diaryrotate(dfolder)
%% DIARYROTATE Rotate diary files



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-03-20
% Changelog:
%   2023-03-20
%       * Initial release



%% Parse arguments

% DIARYROTATE()
% DIARYROTATE(DFOLDER)
narginchk(0, 1);

% DIARYROTATE(___)
nargoutchk(0, 0);

% DIARYROTATE()
if nargin < 1 || isempty(dfolder)
  % Where all diaries are located
  dfolder = fullfile(userpath(), 'diaries');
end



%% Algorithm

% If directory does not exist, stop right here
if 7 ~= exist(dfolder, 'dir')
  return
end

% Get all log files
lfiles = dir(fullfile(dfolder, '*.log'));

% Sort files by name in reverse order (we need to rename the files starting with
% the oldest otherwise we will overwrite everything)
[~, ind] = strnatsortfiles({ lfiles.name }, [], 'ascend');
lfiles = lfiles(ind);

% Loop over all log files
for ifile = numel(lfiles):-1:1
  % Get file
  lfile = lfiles(ifile);
  % Current log file path
  lpath = fullfile(lfile.folder, lfile.name);
  % Build new filename
  [ldir, lname, lext] = fileparts(lpath);
  % Split log file name into its tokens
  ltokens = strsplit(lname, '.');
  % Get number of log from its filename
  if numel(ltokens) > 1
    lnum = str2double(ltokens{2});
  else
    lnum = 0;
  end
  % Build new log file path
  lnpath = fullfile(ldir, sprintf('%s.%d%s', ltokens{1}, lnum + 1, lext));
  % Move file
  movefile(lpath, lnpath);
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
