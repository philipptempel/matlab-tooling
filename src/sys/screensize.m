function ssz = screensize()
%% SCREENSIZE Get screen size
%
% SSZ = SCREENSIZE()
%
% Outputs:
%
%   SS                      1x2 array of current screen's size as
% %                         [ WIDTH , HEIGHT]
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% SCREENSIZE()
narginchk(0, 0);

% SCREENSIZE(___)
% SSZS = SCREENSIZE(___)
nargoutchk(0, 1);



%% Algorithm

% Get screen size
ssz_ = get(groot(), 'ScreenSize');
% sdpi = get(groot(), 'MonitorPositions');

% And return screen width and height only
ssz = ssz_([3,4]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
