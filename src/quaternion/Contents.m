% QUATERNION
%
% Files
%   quat2axang     - Convert quaternion to axis-angle rotation representation
%   quat2conj      - Calculate adjoint quaternion
%   quat2conjmat   - Calculate conjugate quaternion matrix
%   quat2inv       - Calculate inverse quaternion
%   quat2mat       - Calculate quaternion matrix
%   quat2rotm      - Convert Quaternion to rotation matrix
%   quat2tform     - Convert quaternion to homogeneous transformation
%   quat2tpar      - QUAT2TPAR
%   quatadd        - Add two quaternions
%   quatdiv        - Divide two quaternions
%   quatisclose    - Check if two quaternions are close to each other
%   quatmul        - Multiply two quaternions
%   quatnorm       - Calculate norm of quaternion(s)
%   quatnormalized - Normalizes quaternion(s)
%   quatones       - Create array of one-quaternions
%   quatrand       - Create a set of random quaternions
%   quatrandn      - Create normally distributed random quaternions
%   quatsub        - Subtract two quaternions
%   quatzeros      - Create an array of zero-quaternions
