function setutime(filenames, utimes)
%% SETUTIME Set accessed and modified timestamp of filenames
%
% Inputs:
% 
%   FILENAME                Char array of filename for which to set UTime.
%
%   UTIMES                  1x2 array of datetime objects for [ ATIME , MTIME ].
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% SETUTIME(FILENAME, UTIMES)
narginchk(2, 2);

% SETUTIME(___)
nargoutchk(0, 0);



%% Algorithm

% Loop over all files
nfiles = numel(filenames);
for ifile = 1:nfiles
  % Get filename
  filename = filenames{ifile};
  
  % Skip if it doesn't exist
  if 2 ~= exist(filename, 'file')
    continue
  end
  
  pyUtime(fullpath(filename), utimes(ifile,:));
  
end


end


function pyUtime(filename, utimes)
%% PYUTIME


try
  py.os.utime(filename, py.tuple(uint64(posixtime(utimes))));
  
catch me
  throwAsCaller(me);
  
end

end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
