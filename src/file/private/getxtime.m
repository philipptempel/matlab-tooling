function xt = getxtime(filename, timetype, varargin)
%% GETXTIME
%
% Inputs:
% 
%   FILENAME                Description of argument FILENAME
%
%   TIMETYPE                Description of argument TIMETYPE
%
% Outputs:
%
%   XTIME                   Description of argument XTIME
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% GETXTIME(TIMETYPE, FILENAME)
% GETXTIME(___, Name, Value, ...)
narginchk(2, Inf);

% GETXTIME(___)
% XTIME = GETXTIME(___)
nargoutchk(0, 1);



%% Algorithm

% Ensure we always loop over an array of filenames
if ~iscell(filename)
  filenames = { filename };
else
  filenames = filename;
end

% Count
nfilenames = numel(filenames);

% Initialize output
xt_ = repmat({unixepoch()}, size(filenames));

% Loop over all files
for ifilename = 1:nfilenames
  % Get filename
  filename_ = filenames{ifilename};
  
  % Skip checking if file does not exist
  if 2 ~= exist(filename_, 'file')
    continue
  end

  xt_{ifilename} = pyXtime(filename_, timetype, varargin{:});
  
end

xt = reshape(cat(1, xt_{:}), size(filenames));


end


function xt = pyXtime(filename, timetype, varargin)
%% PYXTIME



% Dispatch
try
  % We dispatch to using python from MATLAB as this is the most OS-aware
  % implementation there exists.
  % @see https://stackoverflow.com/questions/23179173/how-to-get-time-of-creation-of-a-file-matlab
  xt = datetime( ...
      feval([ 'py.os.path.get' , timetype ], filename) ...
    , 'ConvertFrom'     , 'posixtime' ...
    , 'TicksPerSecond'  , 1 ...
    , 'Format'          , 'dd-MMM-yyyy HH:mm:ss.SSS' ...
    , varargin{:} ...
  );
  
catch
  xt = unixepoch();
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
