function setatime(filename, atime)
%% SETATIME Set accessed timestamp of file(s)
%
% SETATIME(FILENAME, ATIME) sets the accessed timestamp of file FILENAME to
% ATIME.
%
% Inputs:
%
%   FILENAME                Char array of a single filename or 1xN cell array of
%                           filenames.
% 
%   ATIME                   Datetime object or 1xN array of datetime objects.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   GETATIME SETMTIME



%% Parse arguments

% SETATIME(FILENAME, ATIME)
narginchk(2, 2);

% SETATIME(___)
nargoutchk(0, 0);



%% Algorithm

% Ensure both FILENAME and ATIME are arrays of the size
if ~iscell(filename)
  filename = { filename };
end
if ~isvector(atime)
  atime = repmat(atime, size(filename));
end

% First, get MTIMEs of all files
mtime = getmtime(filename);

% Then dispatch to a generic wrapper function
setutime(reshape(filename, 1, []), [ mtime , atime ]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
