function mt = getmtime(filename, varargin)
%% GETMTIME Get modified timestamp of a file
%
% MT = GETMTIME(FILENAME) gets modified timestamp of file FILENAME.
%
% Inputs:
%
%   FILENAME                Char or 1xN cell array of chars depicting relative
%                           or absolute filenames. Beware that relative
%                           filenames are relative with respect to the current
%                           execution scope thus may lead to unexpected
%                           behavior.
%
% Outputs:
%
%   MT                      Scalar DATETIME object or 1xN cell array of
%                           DATETIMES.
%
% See also:
%   DATETIME ATIME CTIME
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% GETMTIME(FILENAME)
% GETMTIME(___, Name, Value, ...)
narginchk(1, Inf);

% GETMTIME(___)
% MT = GETMTIME(___)
nargoutchk(0, 1);



%% Algorithm

% Call private wrapper function
mt = getxtime(filename, 'mtime', varargin{:});


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
