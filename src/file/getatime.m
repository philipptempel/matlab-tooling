function at = getatime(filename, varargin)
%% GETATIME Get accessed timestamp of a file
%
% AT = GETATIME(FILENAME) gets accessed timestamp of file FILENAME.
%
% Inputs:
%
%   FILENAME                Char or 1xN cell array of chars depicting relative
%                           or absolute filenames. Beware that relative
%                           filenames are relative with respect to the current
%                           execution scope thus may lead to unexpected
%                           behavior.
%
% Outputs:
%
%   AT                      Scalar DATETIME object or 1xN cell array of
%                           DATETIMES.
%
% See also:
%   DATETIME CTIME MTIME
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% GETATIME(FILENAME)
% GETATIME(___, Name, Value, ...)
narginchk(1, Inf);

% GETATIME(___)
% MT = GETATIME(___)
nargoutchk(0, 1);



%% Algorithm

% Call private wrapper function
at = getxtime(filename, 'atime', varargin{:});


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
