function setmtime(filename, mtime)
%% SETMTIME Set modified timestamp of file(s)
%
% SETMTIME(FILENAME, MTIME) sets the modified timestamp of file FILENAME to
% MTIME.
%
% Inputs:
%
%   FILENAME                Char array of a single filename or 1xN cell array of
%                           filenames.
% 
%   MTIME                   Datetime object or 1xN array of datetime objects.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   GETMTIME SETATIME



%% Parse arguments

% SETMTIME(FILENAME, MTIME)
narginchk(2, 2);

% SETMTIME(___)
nargoutchk(0, 0);



%% Algorithm

% Ensure both FILENAME and MTIME are arrays of the size
if ~iscell(filename)
  filename = { filename };
end
if ~isvector(mtime)
  mtime = repmat(mtime, size(filename));
end

% First, get ATIMEs of all files
atime = getatime(filename);

% Then dispatch to a generic wrapper function
setutime(reshape(filename, 1, []), [ atime , mtime ]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
