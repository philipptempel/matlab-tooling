function ct = getctime(filename, varargin)
%% GETCTIME Get changed timestamp of a file
%
% CT = GETCTIME(FILENAME) gets changed timestamp of file FILENAME.
%
% Inputs:
%
%   FILENAME                Char or 1xN cell array of chars depicting relative
%                           or absolute filenames. Beware that relative
%                           filenames are relative with respect to the current
%                           execution scope thus may lead to unexpected
%                           behavior.
%
% Outputs:
%
%   CT                      Scalar DATETIME object or 1xN cell array of
%                           DATETIMES.
%
% See also:
%   DATETIME ATIME MTIME
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% GETCTIME(FILENAME)
% GETCTIME(___, Name, Value, ...)
narginchk(1, Inf);

% GETCTIME(___)
% CT = GETCTIME(___)
nargoutchk(0, 1);



%% Algorithm

% Call private wrapper function
ct = getxtime(filename, 'ctime', varargin{:});


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
