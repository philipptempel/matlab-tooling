function flag = isabsolutepath(p)
%% ISABSOLUTEPATH Check if a path is absolute or relative
%
% Inputs:
%
%   P                       Description of argument P
%
% Outputs:
%
%   FLAG                    Description of argument FLAG



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-06-02
% Changelog:
%   2023-06-02
%       * Initial release



%% Parse arguments



%% Algorithm



end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
