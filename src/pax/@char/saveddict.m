function [dict, prefix, suffix] = saveddict(sn)
%% SAVEDDICT
%
% DICT = SAVEDDICT(SAVENAME)
%
% [DICT, PREFIX] = SAVEDDICT(SAVENAME)
%
% [DICT, PREFIX, SUFFIX] = SAVEDDICT(SAVENAME)
%
% Inputs:
%
%   SN                      Description of argument SN
%
% Outputs:
%
%   DICT                    Description of argument DICT
% 
%   PREFIX                  Description of argument PREFIX
% 
%   SUFFIX                  Description of argument SUFFIX



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-05-30
% Changelog:
%   2023-05-30
%       * Initial release



%% Parse arguments

% SAVEDDICT(SAVENAME)
narginchk(1, 1);

% SAVEDDICT(___)
% DICT = SAVEDDICT(___)
% [DICT, PREFIX] = SAVEDDICT(___)
% [DICT, PREFIX, SUFFIX] = SAVEDDICT(___)
nargoutchk(0, 3);



%% Algorithm

% Split save name by assumed connector
c = strsplit(sn, '_');

% Check if first entry contains the "EQUAL" symbol
prefix = '';
if ~contains(c{1}, '=')
  % Does not, then extract the prefix and remove the field
  prefix = [ c{1} , '_' ];
  c(1) = [];
  
end

% Check if last entry contains the "EQUAL" symbol
suffix = '';
if ~contains(c{end}, '=')
  % Does not, then extract the suffix and remove the field
  suffix = [ '_' , c{end} ];
  c(end) = [];
  
end

% Count fields and create output structure
nf = numel(c);
dict = struct();

% Loop over all fields
for ik = 1:nf
  c_ = c{ik};
  % Split along "EQUAL" symbol
  nv = strsplit(c_, '=');
  f = nv{1};
  v = nv{2};
  
  % Parse value into numeric value if possible
  v_ = str2double(v);
  if ~isnan(v_)
    v = v_;
  end
  
  % And push entry into dictionary
  dict.(f) = v;
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
