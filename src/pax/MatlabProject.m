classdef MatlabProject < mlp.internal.InternalAccess
  %% MATLABPROJECT
  
  
  
  %% PUBLIC CONSTANT VARIABLES
  properties ( Constant )
    
    % List of default directories to use in project
    DefaultDirectories = { 'src' , 'mex' , 'dev' , 'local' };
    
  end
  
  
  
  %% PUBLIC READ-ONLY PROPERTIES
  properties ( SetAccess = immutable )
    
    Root (1,:) char = pwd()
    
  end
  
  
  
  %% PUBLIC PROPERTIES
  properties ( Dependent )
    
    Name
    
    UUID
    
    Version
    
    Description
    
    Homepage
    
    License
    
    Readme
    
    Authors
    
    Autoload
    
    Startup
    
    Finish
    
  end
  
  
  
  %% DEPENDENT READ-ONLY PROPERTIES
  properties ( Dependent , Access = { ?mlp.internal.InternalAccess } )
    
    IsNew
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Access = { ?mlp.internal.InternalAccess } )
    
    Config (1,1) mlp.MatlabProjectConfig
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = MatlabProject(rwd)
      %% MATLABPROJECT
      
      
      
      obj@mlp.internal.InternalAccess();
      obj.Root = rwd;
      
      obj.Config = mlp.MatlabProjectConfig.fromFile( ...
          root(obj, 'MatlabProject.yaml') ...
        , 'Parent', obj ...
      );
      
      % New projects will get a (hopefully) unique UUID
      if isnew(obj)
        obj.UUID = uuid();
      end
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Name(obj)
      %% GET.NAME
      
      
      
      v = obj.Config.Name;
      
    end
    
    
    function v = get.UUID(obj)
      %% GET.UUID
      
      
      
      v = obj.Config.UUID;
      
    end
    
    
    function v = get.Version(obj)
      %% GET.VERSION
      
      
      
      v = obj.Config.Version;
      
    end
    
    
    function v = get.Description(obj)
      %% GET.DESCRIPTION
      
      
      
      v = obj.Config.Description;
      
    end
    
    
    function v = get.Homepage(obj)
      %% GET.HOMEPAGE
      
      
      
      v = obj.Config.Homepage;
      
    end
    
    
    function v = get.License(obj)
      %% GET.LICENSE
      
      
      
      v = obj.Config.License;
      
    end
    
    
    function v = get.Readme(obj)
      %% GET.README
      
      
      
      v = obj.Config.Readme;
      
    end
    
    
    function v = get.Authors(obj)
      %% GET.AUTHORS
      
      
      
      v = obj.Config.Authors;
      
    end
    
    
    function v = get.Autoload(obj)
      %% GET.AUTOLOAD
      
      
      
      v = obj.Config.Autoload;
      
    end
    
    
    function v = get.Startup(obj)
      %% GET.STARTUP
      
      
      
      v = obj.Config.Startup;
      
    end
    
    
    function v = get.Finish(obj)
      %% GET.FINISH
      
      
      
      v = obj.Config.Finish;
      
    end
    
    
    function v = get.IsNew(obj)
      %% GET.ISNEW
      
      
      
      v = isnew(obj);
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Name(obj, v)
      %% SET.NAME
      
      
      
      obj.Config.Name = v;
      
    end
    
    
    function set.UUID(obj, v)
      %% SET.UUID
      
      
      
      obj.Config.UUID = v;
      
    end
    
    
    function set.Version(obj, v)
      %% SET.VERSION
      
      
      
      obj.Config.Version = v;
      
    end
    
    
    function set.Description(obj, v)
      %% SET.DESCRIPTION
      
      
      
      obj.Config.Description = v;
      
    end
    
    
    function set.Homepage(obj, v)
      %% SET.HOMEPAGE
      
      
      
      obj.Config.Homepage = v;
      
      
    end
    
    
    function set.License(obj, v)
      %% SET.LICENSE
      
      
      
      obj.Config.License = v;
      
    end
    
    
    function set.Readme(obj, v)
      %% SET.README
      
      
      
      obj.Config.Readme = v;
      
    end
    
    
    function set.Authors(obj, v)
      %% SET.AUTHORS
      
      
      
      obj.Config.Authors = v;
     
    end
    
    
    function set.Autoload(obj, v)
      %% SET.AUTOLOAD
      
      
      
      obj.Config.Autoload = v;
      
    end
    
    
    function set.Startup(obj, v)
      %% SET.STARTUP
      
      
      
      obj.Config.Startup = v;
      
    end
    
    
    function set.Finish(obj, v)
      %% SET.FINISH
      
      
      
      obj.Config.Finish = v;
      
    end
    
  end
  
  
  
  %% CONVERSION METHODS
  methods
    
    function s = struct(obj)
      %% STRUCT
      
      
      
      s = struct();
      s.name        = obj.Name;
      s.uuid        = obj.UUID;
      s.version     = obj.Version;
      s.description = obj.Description;
      s.readme      = obj.Readme;
      s.homepage    = obj.Homepage;
      s.license     = obj.License;
      s.authors     = obj.Authors;
      s.autoload    = obj.Autoload;
      s.startup     = obj.Startup;
      s.finish      = obj.Finish;
      
    end
    
  end
  
  
  
  %% PATH METHODS
  methods
    
    function f = data(obj, varargin)
      %% DATA
      
      
      
      f = root(obj, 'data', varargin{:});
      
    end
    
    
    function f = dev(obj, varargin)
      %% DEV
      
      
      
      f = root(obj, 'dev', varargin{:});
      
    end
    
    
    function f = fullfile(obj, varargin)
      %% FULLFILE
      
      
      
      f = fullfile(obj.Root, varargin{:});
      
    end
    
    
    function f = local(obj, varargin)
      %% LOCAL
      
      
      
      f = root(obj, 'local', varargin{:});
      
    end
    
    
    function f = mex(obj, varargin)
      %% MEX
      
      
      
      f = root(obj, 'mex', varargin{:});
      
    end
    
    
    function f = root(obj, varargin)
      %% ROOT
      
      
      
      f = fullfile(obj, varargin{:});
      
    end
    
    
    function f = src(obj, varargin)
      %% SRC
      
      
      
      f = root(obj, 'src', varargin{:});
      
    end
    
    
    function f = test(obj, varargin)
      %% TEST
      
      
      
      f = root(obj, 'test', varargin{:});
      
    end
    
  end
  
  
  
  %% PROJECT METHODS
  methods
    
    function flag = isnew(obj)
      %% ISNEW
      
      
      
      flag = 2 ~= exist(root(obj, 'MatlabProject.yaml'), 'file');
      
    end
    
    
    function run(obj, varargin)
      %% RUN
      
      
      
      % Get path to the desired script/function
      p = root(obj, varargin{:});
      
      % Ensure file to run exists
      if 2 ~= exist(p, 'file')
        return
      end
      
      % Split file path into its components
      [fdir, fname, ~] = fileparts(p);
      
      % Change to the file's directory ensuring that we will always change back
      % to the current working directory in case of any errors. This is
      % different to `RUN` behavior which does not `CD` back if the executed
      % script/function changes directories
      cwd = pwd();
      cd(fdir);
      coWd = onCleanup(@() cd(cwd));
      
      % Run the script/function
      run(fname);
      
    end
    
    
    function activate(obj)
      %% ACTIVATE
      
      
      
      addpath(obj);
      
      startup(obj);
      
    end
    
    
    function deactivate(obj)
      %% DEACTIVATE
      
      
      
      finish(obj);
      
      rmpath(obj);
      
    end
    
    
    function finish(obj)
      %% FINISH
      
      
      
      % Safely run the project's `finish` script/function
      run(obj, 'finish.m');
      
    end
    
    
    function startup(obj)
      %% STARTUP
      
      
      
      % Safely run the project's `startup` script/function
      run(obj, 'startup.m');
      
    end
    
    
    function addpath(obj)
      %% ADDPATH
      
      
      
      % First, load everything inside autoload
      nal = numel(obj.Autoload);
      for ial = 1:nal
        % Get autoload config
        al = char(obj.Autoload{ial});
        
        % Skip empty entries
        if isempty(al)
          continue
        end
        
        % Check if the directory is a `MatlabProject`
        if 2 == exist(fullfile(al, 'MatlabProject.yaml'), 'file')
          % Then start up this project
          addpath(MatlabProject.at(al));
          
          continue
          
        end
        
        % Not at MatlabProject at the directory...
        % First, ensure al is an absolute path
        if ~java.io.File(al).isAbsolute()
          al = fullfile(obj, al);
        end
        % Check if `AL` ends with `*`, then we will add everything from there to
        % the path
        if endsWith(al, '*')
          pth = genpath(al);
          
        else
          pth = al;
          
        end
        
        % Finally, add the generated paths to MATLAB's search path
        addpath(pth);
        
      end
      
      % Lastly, add all default directories to the path
      dfdir = obj.DefaultDirectories;
      ndfdir = numel(dfdir);
      
      for idfdir = 1:1:ndfdir
        addpath(genpath(root(obj, dfdir{idfdir})));
      end
      
    end
    
    
    function rmpath(obj)
      %% RMPATH
      
      
      
      % First, remove all directories inside `Autoload'
      nal = numel(obj.Autoload);
      for ial = nal:-1:1
        % Get autoload config
        al = char(obj.Autoload{ial});
        
        % Skip empty entries
        if isempty(al)
          continue
        end
        
        % Check if the directory is a `MatlabProject`
        if 2 == exist(fullfile(al, 'MatlabProject.yaml'), 'file')
          % Then start up this project
          rmpath(MatlabProject.at(al));
          
          continue
          
        end
        
        % Not at MatlabProject at the directory...
        % First, ensure al is an absolute path
        if ~java.io.File(al).isAbsolute()
          al = fullfile(obj, al);
        end
        % Check if `AL` ends with `*`, then we will add everything from there to
        % the path
        if endsWith(al, '*')
          pth = genpath(al);
        else
          pth = al;
        end
        
        % Finally, add the generated paths to MATLAB's search path
        rmpath(pth);
        
      end
      
      % Lastly, remove all default directories to the path
      dfdir = flip(obj.DefaultDirectories);
      ndfdir = numel(dfdir);
      
      for idfdir = 1:1:ndfdir
        rmpath(genpath(root(obj, dfdir{idfdir})));
      end
      
    end
    
  end
  
  
  
  %% STATIC METHODS
  methods ( Static )
    
    function mp = this()
      %% THIS
      
      
      
      % Initialize empty project
      mp = [];
      
      % First, check in the caller's directory tree starting at the most top
      % level caller then descending down the caller stack
      caller = dbstack(2, '-completenames');
      ncaller = numel(caller);
      if ncaller > 0
        for icaller = ncaller:-1:1
          mp = MatlabProject.at(fileparts(caller(icaller).file));
          if ~isempty(mp)
            return
          end
        end
      end
      
      % Then, check in the current working directory's tree and its parents'
      if isempty(mp)
        mp = MatlabProject.at(pwd());
        
      end
      
      % Nothing found, so create a new project at the current location/path
      if isempty(mp)
        mp = MatlabProject(pwd());
        
      end
      
    end
    
    
    function mp = at(rwd)
      %% AT
      
      
      
      % Convert path into a JAVA file object
      rwd = java.io.File(fullpath(rwd));
      froots = rwd.listRoots();
      
      % Loop indicator
      stop = false;
      
      % Loop up one directory at a time
      while ~stop
        found = isempty(rwd) || ... # skip if path empty
          ... # skip if current directory matches file system root
          any(arrayfun(@(r) strcmp(r, rwd), froots)) || ...
          ... % skip if found a `MatlabProject.yaml` file
          2 == exist(fullfile(char(rwd), 'MatlabProject.yaml'), 'file');
        
        if found
          stop = true;
          
        else
          stop = false;
          rwd = java.io.File(rwd.getParent());
          
        end
        
      end
      
      % Return a project object if we found a `MatlabProject.yaml` file and if it's not
      % the root directory
      if ~( isempty(rwd) || any(arrayfun(@(r) strcmp(r, rwd), froots)) )
          mp = MatlabProject(fullfile(char(rwd)));
          
      % Nothing found
      else
        mp = [];
        
      end
      
    end
    
  end
  
end
