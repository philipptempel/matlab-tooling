classdef MatlabProjectConfig < mlp.internal.InternalAccess ...
    & matlab.mixin.SetGet
  %% MATLABPROJECTCONFIG
  
  
  
  %% PUBLIC PROPERTIES
  properties ( Access = { ?mlp.internal.InternalAccess } )
    
    Name          (1,:) char = ''
    
    UUID          (1,:) char = ''
    
    Version       (1,:) char = ''
    
    Description   (:,:) char = ''
    
    Homepage      (1,:) char = ''
    
    License       (1,:) char = ''
    
    Readme        (1,:) char = ''
    
    Authors       (1,:) cell = cell(1, 0)
    
    Autoload      (1,:) cell = cell(1, 0)
    
    Startup       (1,:) char = 'startup.m'
    
    Finish        (1,:) char = 'finish.m'
    
    Parent        (1,1) 
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = MatlabProjectConfig(varargin)
      %% MATLABPROJECTCONFIG
      
      
      
      obj@mlp.internal.InternalAccess();
      obj@matlab.mixin.SetGet();
      
      if nargin > 0
        set(obj, varargin{:});
      end
      
    end
    
  end
  
  
  
  %% PUBLIC STATIC METHODS
  methods ( Static )
    
    function obj = fromFile(file, varargin)
      %% FROMFILE
      
      
      
      % Check path to YAML file exists
      if 2 == exist(file, 'file')
        s = yaml.loadFile(file);
        
      % YAML file does not exist
      else
        s = struct();
        
      end
      
      obj = mlp.MatlabProjectConfig(s, varargin{:});
      
    end
    
  end
  
end
