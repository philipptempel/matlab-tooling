function sn = savename(dict, prefix, suffix, varargin)
%% SAVENAME
%
% Inputs:
% 
%   DICT                    Description of argument DICT
%
%   PREFIX                  Description of argument PREFIX
% 
%   SUFFIX                  Description of argument SUFFIX
%
% Outputs:
%
%   SN                      Description of argument SN
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% SAVENAME(DICT)
% SAVENAME(DICT, PREFIX)
% SAVENAME(DICT, PREFIX, SUFFIX)
% SAVENAME(___, Name, Value, ...)
narginchk(1, Inf);

% SAVENAME(___)
% SN = SAVENAME(___)
nargoutchk(0, 1);

% SAVENAME(DICT)
if nargin < 2 || isempty(prefix)
  prefix = '';
end

% SAVENAME(DICT, PREFIX)
if nargin < 3 || isempty(suffix)
  suffix = '';
end

% Parse options
options = nvpairs2struct(varargin{:});

% Default options
defaultopts = struct( ...
    'Connector' , '_' ...
  , 'Digits'    , 4 ...
  , 'SigDigits' , [] ...
  , 'Sort'      , true ...
);



%% Algorithm

% Get field names and values from dictionary
f = fieldnames(dict);
v = struct2cell(dict);
% Sort structure fields and associated values in natural string sort order
if matlab.lang.OnOffSwitchState(optsget(options, 'Sort', defaultopts.Sort, 'fast'))
  [f, ind] = strnatsort(f);
  v = v(ind);
end

% Number of fields
nf = numel(f);

digits    = optsget(options, 'Digits', defaultopts.Digits, 'fast');
sigdigits = optsget(options, 'SigDigits', defaultopts.SigDigits, 'fast');

% Format string for numeric non-integer numbers
fstrnumeric = [ '%+' , num2str(sigdigits) , '.' , num2str(digits) , 'f' ];

% Initialize
c = cell(1, nf);

% Loop over all keys
for ik = 1:nf
  % Get the current key and value
  f_ = f{ik};
  v_ = v{ik};
  
  % Check if value is a full integer
  if isa(v_, 'char')
    c{ik} = sprintf('%s=%s', f_, v_);

  elseif isa(v_, 'integer') || mod(v_, 1) == 0
    c{ik} = sprintf('%s=%g', f_, v_);

  else
    c{ik} = sprintf([ '%s=' , fstrnumeric ], f_, v_);

  end
  
end

% Finally concatenat all pieces
sn = [ char(prefix) , strjoin(c, optsget(options, 'Connector', defaultopts.Connector, 'fast')) , char(suffix) ];


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
