function [args, h1args, f] = funcgenParseVararg(t, args)
%% FUNCGENPARSEVARARG
%
% [ARGS, H1ARGS, F] = FUNCGENPARSEVARARG(T, ARGS)



% H1 arguments are the same as signature arguments
h1args = args;
% Find the index of 'varargin' in the input argument names
idxS = find(strcmpi(args, sprintf('vararg%s', t)));
f = ~isempty(idxS) && numel(args) >= idxS;
% Reject everything after 'varargin'
if f
  h1args(idxS:end) = [];
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changes" section of the header.
