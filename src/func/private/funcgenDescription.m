function description = funcgenDescription(description, argsin, argsout)
%% FUNCGENDESCRIPTION
%
% DESCRIPTION = FUNCGENDESCRIPTION(DESCRIPTION, ARGS_IN, ARGS_OUT)



% Holds the formatted list entries of inargs and outargs
argsin_list = cell(numel(argsin), min(numel(argsin), 1));
argsout_list = cell(numel(argsout), min(numel(argsout), 1));

% Determine longest argument name for input
argsin_length = max(cellfun(@(x) length(x), argsin));
if isempty(argsin_length)
    argsin_length = 0;
end
% and output
argsout_length = max(cellfun(@(x) length(x), argsout));
if isempty(argsout_length)
    argsout_length = 0;
end

% Determine the longer argument names: input or output?
args_length = max([argsin_length, argsout_length]);
% Get the index of the next column (dividable by 4) but be at least at
% column 25
ncol_spacer = max([25, 4*ceil((args_length + 1)/4) + 1]);

% First, create a lits of in arguments
if ~isempty(argsin)
    % Prepend comment char and whitespace before uppercased argument
    % name, append whitespace up to filling column and a placeholder at
    % the end
    argsin_list = cellfun(@(x) sprintf('%%   %s%s%s %s', upper(x), repmat(' ', 1, ncol_spacer - length(x) - 1), 'Description of argument', upper(x)), argsin, 'Uniform', false);
end

% Second, create a lits of out arguments
if ~isempty(argsout)
    % Prepend comment char and whitespace before uppercased argument
    % name, append whitespace up to filling column and a placeholder at
    % the end
    argsout_list = cellfun(@(x) sprintf('%%   %s%s%s %s', upper(x), repmat(' ', 1, ncol_spacer - length(x) - 1), 'Description of argument', upper(x)), argsout, 'Uniform', false);
end

% Append list of input arguments?
if ~isempty(argsin_list)
    description = [ ...
        description   , newline() ...
      , '%'           , newline() ...
      , '% Inputs:'   , newline() ...
      , '%'           , newline() ...
      , strjoin(argsin_list, [ newline() , '% ' , newline() ]) ...
    ];
end

% Append list of output arguments?
if ~isempty(argsout_list)
    description = [ ...
        description   , newline() ...
      , '%'           , newline() ...
      , '% Outputs:'  , newline() ...
      , '%'           , newline() ...
      , strjoin(argsout_list, [ newline() , '% ' , newline() ]) ...
    ];
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changes" section of the header.
