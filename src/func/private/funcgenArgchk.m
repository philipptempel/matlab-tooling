function a = funcgenArgchk(t, n)
%% FUNCGENARGCHK
%
% A = FUNCGENARGCHK(T, N)



if ~isempty(n)
  a = sprintf('narg%schk(%d, %d);', t, n(1), n(2));
  
  if strcmpi(t, 'in')
    a = sprintf('\n%s\n', a);
    
  elseif strcmpi(t, 'out')
    a = sprintf('%s\n\n', a);
    
  end
  
else
  a = '';
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changes" section of the header.
