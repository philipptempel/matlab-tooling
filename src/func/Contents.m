% FUNC
%
% Files
%   cpfun         - renames the function to a new name
%   cpscrpt       - renames the script to a new name
%   funcname      - returns the current function's name
%   isfunction    - - true for valid matlab functions
%   mkfun         - creates a new function file based on a template
%   mkscrpt       - creates a new script file based on a template
%   mvfun         - renames the function to a new name
%   mvscrpt       - renames the script to a new name
%   noop          - A no-op function in MATLAB
%   onoffstate    - parses the toggle arg to a valid and unified char.
%   packageroot   - Determine the root directory of the given package.
%   splitaxesargs - calls axescheck for a list with an object inside
%   ternary       - Implementation of a ternary operator in MATLAB
