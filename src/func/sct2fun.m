function sct2fun(filename)
%% SCT2FUN Turn a MATLAB script into a function
%
% SCT2FUN(FILENAME) turns the script located at FILENAME into a
% function. In most simple terms, it will prepend a function header and function
% footer.
%
% Inputs:
%
%   FILENAME                Description of argument FILENAME
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% SCT2FUN(FILENAME)
narginchk(1, 1);

% SCT2FUN(___)
nargoutchk(0, 1);



%% Algorithm

% Check source file exists
if 2 ~= exist(filename, 'file')
  throw(MException('PHILIPPTEMPEL:FUNC:SCT2FUN:FileNotFound', 'File not found at %s.', filename));
end

% Split filename into its path parts
[fdir, fname, fext] = fileparts(filename);
if isempty(fdir)
  fdir = pwd();
end
if isempty(fext)
  fext = '.m';
end

% Build correct path to source file
path_src = fullfile(fdir, [ fname , fext ]);
% Path to a temporary file
path_tmp = tempname();

% First, read the original file
try
  [fid, errmsg] = fopen(path_src);
  if fid < 0
    throw(MException('MATLAB:FileIO:InvalidFid', errmsg));
  end
  coClose = onCleanup(@() fclose(fid));
  
  content = fileread(path_src);
  
  if isempty(content)
    content = newline();
  end
  
catch me
  throwAsCaller(me);
  
end

func_header = [ ...
    'function ' , fname , '()' , newline() ...
  , '%% ' , upper(fname) , newline() ...
  , newline() ...
  , newline() ...
  , newline() ...
  , '%% Algorithm' ...
  , newline() ...
];
func_footer = [ ...
    newline() , newline() ...
  , 'end' , newline() ...
  , newline() ...
  , '%------------- END OF CODE --------------' , newline() ...
  , '% Please send suggestions for improvement of this file to the original author as' , newline() ...
  , '% can be found in the header. Your contribution towards improving this function' , newline() ...
  , '% will be acknowledged.' ...
];

% Write original text to new file
try
  [fid, errmsg] = fopen(path_tmp, 'w+');
  if fid < 0
    throw(MException('MATLAB:FileIO:InvalidFid', errmsg));
  end
  coFid = onCleanup(@() fclose(fid));
  
  % Write content
  fprintf(fid, '%s%s', func_header, newline());
  fprintf(fid, '%s', content);
  fprintf(fid, '%s%s', func_footer, newline());
    
  % Ensure an EOF-newline
  if numel(content) && ~strcmp(content(end), newline())
    fprintf(fid, '%s', newline());
  end
  
  % And move the temporary file over the source file
  movefile(path_tmp, path_src);
  
catch me
  throwAsCaller(me);
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
