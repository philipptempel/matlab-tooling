% STR
%
% Files
%   num2prefix      - converts numbers to their SI unit prefix.
%   pluralize       - Select the plural or singular form of the argument
%   randstr         - Create a random text string
%   strcase         - Change string case
%   strdamlevdist   - Calculate the Damerau–Levenshtein distance between two strings
%   strdist         - computes distances between strings
%   strescape       - Escape a string
%   strhammdist     - Calculate Hamming distance between two strings
%   strlcfirst      - lower cases the frist character of the string(s)
%   strnatsort      - Alphanumeric / Natural-Order sort the strings in a cell array of strings.
%   strnatsortfiles - Alphanumeric / Natural-Order sort of a cell array of filenames/filepaths.
%   strucfirst      - upper cases the frist character of the string(s)
%   strucwords      - uppercases each word of the given strings
