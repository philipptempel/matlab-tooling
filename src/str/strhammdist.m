function d = strhammdist(a, b)%#codegen
%% STRHAMMDIST Calculate Hamming distance between two strings
%
% D = STRHAMMDIST(A, B) calculates the Hamming distance between string A and B.
%
% Inputs:
%
%   A                       First character string.
% 
%   B                       Second character string.
%
% Outputs:
%
%   D                       Hamming distance between string A and string B.



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-03-03
% Changelog:
%   2023-03-03
%       * Initial release



%% Parse arguments

% STRHAMMDIST(A, B)
narginchk(2, 2);

% STRHAMMDIST(___)
% D = STRHAMMDIST(___)
nargoutchk(0, 1);



%% Algorithm

na = length(a);
nb = length(b);
ndf = nb - na;

str1rep = -ones(ndf + 1, nb);
str1rep(sub2ind([ ndf + 1 , nb ], reshape(repmat(1:(ndf + 1), [ na , 1 ]), [], 1), reshape(bsxfun(@plus, (1:na)', 0:ndf), [], 1))) = repmat(a, [ 1 , ndf + 1 ]);

d = min(sum(str1rep ~= double(repmat(b, [ ndf + 1 , 1 ])), 2));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
