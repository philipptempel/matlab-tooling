function uc = strucfirst(varargin)
%% STRUCFIRST upper cases the frist character of the string(s)
%
% Inputs:
%
%   STRING      Description of argument STRING
%
% Outputs:
%
%   UCED        Description of argument UCED
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Algorithm

% Get all strings
uc_ = varargin;

% Loop over all strings
for iarg = 1:nargin
  if ~isempty(varargin{iarg})
    uc_{iarg}(1) = upper(uc_{iarg}(1));
  end
end

% If one string given, return one string
if nargin == 1
  uc = uc_{1};
  
% Multiple strings given, so return all of them
else
  uc = uc_;
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changes" section of the header.
