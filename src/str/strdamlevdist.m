function d = strdamlevdist(a, b)%#codegen
%% STRDAMLEVDIST Calculate the Damerau–Levenshtein distance between two strings
%
% D = STRDAMLEVDIST(A, B) calculates the string distance between the two strings
% A and B allowing for
%   deletion
%   insertion
%   transposition
% of characters
%
% Inputs:
%
%   A                       Character array.
% 
%   B                       Character array.
%
% Outputs:
%
%   D                       Distance between the two words.



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2023-03-03
% Changelog:
%   2023-03-03
%       * Initial release



%% Parse arguments

% STRDAMLEVDIST(A, B)
narginchk(2, 2);

% STRDAMLEVDIST(___)
% D = STRDAMLEVDIST(___)
nargoutchk(0, 1);



%% Algorithm

na = numel(a);
nb = numel(b);

d_ = zeros(na + 1, nb + 1);
d_(:,1) = 0:na;
d_(1,:) = 0:nb;

for i = 1:na
  for j = 1:nb
    if isequal(a(i), b(j))
      cost = 0;
    else
      cost = 1;
    end
    
    d_(i+1,j+1) = min([ ...
          ... % deletion
          d_(i-1+1,j+1) + 1 ...
        ... % insertion
        , d_(i+1,j-1+1) + 1 ...
        ... % substitution
        , d_(i-1+1,j-1+1) + cost ...
    ]);
    
    if i > 1 && j > 1 && isequal(a(i), b(j-1)) && isequal(a(i-1), b(j))
      d_(i+1,j+1) = min([ ...
          d_(i+1,j+1) ...
        ... % transposition
        , d_(i-2+1,j-2+1) + 1 ...
      ]);
    
    end
    
  end
  
end

d = d_(na+1,nb+1);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
