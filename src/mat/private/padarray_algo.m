function b = padarray_algo(a, padSize, method, padVal, direction)%#codegen
%% PADARRAY_ALGO Pad array.
%
% B = PADARRAY_AGLO(A,PADSIZE,METHOD,PADVAL,DIRECTION) internal helper function
% for PADARRAY, which performs no input validation.  See the help for PADARRAY
% for the description of input arguments, class support, and examples.


%% Algorithm

if isempty(a)
  
  numDims = numel(padSize);
  sizeB = zeros(1, numDims);

  for k = 1: numDims
    % treat empty matrix similar for any method
    if strcmp(direction, 'both')
      sizeB(k) = size(a, k) + 2 * padSize(k);
    else
      sizeB(k) = size(a, k) +     padSize(k);
    end

  end

  b = mkconstarray(class(a), padVal, sizeB);

% constant value padding with padVal
elseif strcmpi(method, 'constant')
  b = ConstantPad(a, padSize, padVal, direction);

% compute indices then index into input image
else
  aSize = size(a);
  aIdx = getPaddingIndices(aSize, padSize, method, direction);
  b = a(aIdx{:});
  
end

if islogical(a)
  b = logical(b);
end


end


function b = ConstantPad(a, padSize, padVal, direction)
%% CONSTANTPAD


numDims = numel(padSize);

% Form index vectors to subsasgn input array into output array.
% Also compute the size of the output array.
idx   = cell(1, numDims);
sizeB = zeros(1, numDims);
for k = 1:numDims
  M = size(a, k);
  switch direction
    case 'pre'
      idx{k}   = (1:M) + padSize(k);
      sizeB(k) =    M  + padSize(k);
    
    case 'post'
      idx{k}   = 1:M;
      sizeB(k) =   M + padSize(k);
    
    case 'both'
      idx{k}   = (1:M) +     padSize(k);
      sizeB(k) =    M  + 2 * padSize(k);
      
  end
  
end

% Initialize output array with the padding value.  Make sure the
% output array is the same type as the input.
b         = mkconstarray(class(a), padVal, sizeB);
b(idx{:}) = a;

end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
