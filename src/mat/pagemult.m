function ab = pagemult(a, b)%#codegen
%% PAGEMULT Multiplies pages of two arrays
%
% AB = PAGEMULT(A, B) multiplies pages of 3D matrices A and B with each other.
% Only works for 3D matrices, for higher-dimensional matrix-matrix products, use
% MATLAB's built-in pagemtimes`.
%
% Inputs:
%
%   A                   NxMxK array.
%
%   B                   MxLxK array.
%
% Outputs:
%
%   AB                  Page-wise product of A and B i.e., product along the
%                       third dimension.
%
% See also:
%   PAGEMTIMES



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2022-08-11
% Changelog:
%   2022-08-11
%       * Make function compatible with code generation
%   2021-11-19
%       * Initial release



%% Parse arguments

% PAGEMULT(A, B)
narginchk(2, 2);

% PAGEMULT(___)
% AB = PAGEMULT(___)
nargoutchk(0, 1);



%% Algorithm

% Get number of dimensions of both matrices
nda = ndims(a);
ndb = ndims(b);
% Maximum dimension of either matrices
md = max(nda, ndb);

% First, add a singleton dimension after the second dimension into A and B
a = permute(a, [1,2,nda+1,3:nda]);
b = permute(b, [ndb+1,1,2,3:ndb]);

% % If A and B have different dimensions, we will have to match higher dimensions
% if nda ~= ndb
  % Increase maximum dimension counter because we added a dimension
  md = md + 1;
  
  % Get original sizes of A and B so we can inspect their singleton dimensions
  sza = size(a, 1:md);
  szb = size(b, 1:md);
  
  % Initialize REPMAT pattern
  rpa = ones(1, md);
  rpb = ones(1, md);
  
  % We need to repeat singleton dimensions of A or B with the B's or A's size in
  % that dimension
  rpa(sza == 1) = szb(sza == 1);
  rpb(szb == 1) = sza(szb == 1);
  
  % Then, repeat both A and B to the of the same size
  a = repmat(a, rpa);
  b = repmat(b, rpb);
  
% end

% We perform element-wise multiplication of the matrices to get 
ab = permute( ...
    sum( ...
        a .* b ...
      , 2 ...
    ) ...
  , [ 1 , 3:(md+1) , 2 ] ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
