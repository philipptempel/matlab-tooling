% MAT
%
% Files
%   aall          - is a wrapper over recursive calls to all(all(all(....)))
%   aany          - is a wrapper over recursive calls to any(any(any(....)))
%   ascol         - returns elements of array as column vector
%   asrow         - ensures the vector is a row vector
%   asvec         - return elements of array X as a vector
%   bfdcoeffs     - Calculate the Backward Finite-Differences Coefficients
%   bfdmatrix     - 
%   bundle        - Bundle variable arguments together
%   cfdcoeffs     - Calculate the Central Finite-Differences Coefficients
%   cfdmatrix     - 
%   cross2        - Two-dimensional cross product of vectors
%   dct1mtx       - Calculate the matrix of the Discrete Cosine Transform Type 1
%   dct2mtx       - Calculate the matrix of the Discrete Cosine Transform Type 2
%   dct3mtx       - Calculate the matrix of the Discrete Cosine Transform Type 3
%   dct4mtx       - Calculate the matrix of the Discrete Cosine Transform Type 4
%   einsum        - Calculate tensor contraction
%   fdcoeffs      - Calculate finite differences coefficients of arbitrary direction
%   ffdcoeffs     - Calculate the Forward Finite-Differences Coefficients
%   ffdmatrix     - 
%   fnsdim        - First non-singleton dimension.
%   frac          - Return the fractional part of a number
%   hslice        - Hyperslice an array of unknown dimensions
%   inside        - Check if value inside an interval
%   isalnum       - True for alphanumerics (letters and digits).
%   isalpha       - True for letters.
%   isascii       - True for decimal digits.
%   iscolvector   - True for column vector input.
%   isdigit       - True for decimal digits.
%   isdivisibleby - checks if number is divisable by divisor
%   iseven        - checks the given number(s) for being even
%   isint         - checks the given value to be of natural numbers
%   islower       - True for lowercase letters.
%   ismeshgrid    - determines whether grid might come from meshgrid or not
%   ismonotonic   - returns a boolean value indicating whether or not a vector is monotonic.  
%   isnatural     - True for arrays of positive i.e., non-negative natural nmbers.
%   isodd         - checks the given number(s) for being odd
%   isposint      - True for positive integers.
%   isprtchr      - True for printable characters.
%   isrowvector   - True for row vector input.
%   issingular    - Determine singularity of a value
%   issize        - - Check whether the given matrix is of provided size/dimensions
%   issquare      - - Check whether the given matrix is square
%   isupper       - True for uppercase letters.
%   isxdigit      - True for hexadecimal digits.
%   limitnorm     - Limit vector norm to a given value
%   matsplit      - 
%   minmax        - Get Minimum and Maximum of an arra
%   ord           - returns the ordinal for the given number
%   outside       - Check if value is outside of an interval
%   padarray      - Pad an array with certain numbers
%   padcat        - - concatenate vectors with different lengths by padding with NaN
%   pageinv       - Page-wise matrix inverse
%   pagemult      - Multiplies pages of two arrays
%   pages2blkdiag - Turn pages of matrix into block-diagonal blocks.
%   timespace     - creates a properly and evenly spaced vector
%   unbundle      - Unbundle a bundled vector into separate entities
%   unpacknum     - Unpack number into fraction and exponent.
%   wraprad       - Map angles measured in radians to the interval [-pi,pi).
%   xkron         - Kronecker tensor product.
