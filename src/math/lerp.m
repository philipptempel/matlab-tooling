function l = lerp(a, b, t)%#codegen
%% LERP Linear interpolation between two values
%
% LERP(A, B, T) interpolates linearly between A and B at T where T is a scaling
% factor. The function assumes that X(0) = A and X(1) = B; T may be any value
% between -oo and +oo.
%
% Inputs:
%
%   A                   NxK matrix of values to use as start point of
%                       interpolation.
%
%   B                   NxK matrix of values to use as end point of
%                       interpolation.
%
%   T                   1xS vector of values at which to evaluate interpolation.
%
% Outputs:
%
%   L                   NxKxS array of interpolated values.



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2022-02-10
% Changelog:
%   2022-02-10
%       * Fix incorrect use of limiting values to boundaries by possibly
%       assigning the boundaries to an empty array. Now, the code first limits
%       `T` to be within the boundaries of the interval [0, 1], and then
%       performs all calculation
%   2022-01-21
%       * Enforce extrapolation of values is bound by values of A and B for T <
%       0 and T > 1, respectively
%       * Add `NARGINCHK` and `NARGOUTCHK`
%   2021-01-24
%       * Initial release



%% Parse arguments

% LERP(A, B, T)
narginchk(3, 3);

% LERP(___)
% L = LERP(___)
nargoutchk(0, 1);



%% Algorithm

% Get maximum number of dimensions of A and B
nd = max(ndims(a), ndims(b));
% Number of time values
nt = numel(t);

% Sizes of both input arrays
sza = size(a, 1:nd);
szb = size(b, 1:nd);

% Initialize REPMAT pattern
rpa = ones(1, nd);
rpb = ones(1, nd);

% We need to repeat singleton dimensions of A or B with the B's or A's size in
% that dimension
rpa(sza == 1) = szb(sza == 1);
rpb(szb == 1) = sza(szb == 1);

% Repeat A and B have the same size amongst each other and then also along the
% last dimension to match that of the time vector
a = repmat(repmat(a, rpa), [ ones(1, nd) , nt ]);
b = repmat(repmat(b, rpb), [ ones(1, nd) , nt ]);

% Turn T into a row vector, limit it to lie within [ 0 , 1 ], and repeat it for
% all the entries of A (and B consequently)
tn = repmat( ...
    permute( ...
        reshape( ...
            limit(t, 0.00, 1.00) ...
          , [] ...
          , 1 ...
        ) ...
      , [ 2:(nd+1) , 1 ] ...
    ) ...
  , [ sza , 1 ] ...
);

% Interpolate linearly
l_ = ( 1 - tn ).* a + tn .* b;

% Then reshape such that we have the time interpolation along the n+1 dimension
% of the final product
% (1x1, 1x1, 1x1) -> 1x1
% (1x1, 1x1, 1xN) -> 1xN
% (2x1, 2x1, 1x1) -> 2x1
% (2x1, 2x1, 1xN) -> 2xN
% (2x1, 1x2, 1xN) -> 2x2xN
if size(l_, 2) == 1
  l = permute(l_, [ 1 , nd + 1 , 2:nd ]);
  
elseif size(l_, 1) == 1
  l = permute(l_, [ 2:(nd+1) , 1 ]);
  
% No change to shape necessary
else
  l = l_;
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
