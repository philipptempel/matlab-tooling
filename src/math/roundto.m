function vr = roundto(v, f)
%% ROUNDTO Round numbers to next multiple of factor
%
% VR = ROUNDTO(V, F) rounds values of V to next multiple of factor F.
%
% Inputs:
%
%   V                       NxM array of values
% 
%   F                       Scalar factor of which value to round to.
%
% Outputs:
%
%   VR                      Values of V rounded to next factor of F.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ROUNDTO(V, F)
narginchk(2, 2);

% ROUNDTO(___)
% VR = ROUNDTO(___)
nargoutchk(0, 1);



%% Algorithm

% @SEE https://stackoverflow.com/a/16195159
vr = v + mod(f - mod(v, f), f);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
