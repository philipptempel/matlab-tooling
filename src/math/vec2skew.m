function s = vec2skew(v)%#codegen
%% VEC2SKEW Turn vector into its skew-symmetrix matrix form
%
% Inputs:
%
%   V                       Description of argument V
%
% Outputs:
%
%   S                       Description of argument S



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>
% Date: 2022-10-18
% Changelog:
%   2022-10-18
%       * Initial release



%% Parse arguments

% VEC2SKEW(V)
narginchk(1, 1);

% VEC2SKEW(___)
% S = VEC2SKEW(___)
nargoutchk(0, 1);



%% Algorithm

% Ensure V is a column vector
if isvector(v)
  v_ = reshape(v, [], 1);

else
  v_ = v;
  
end

szv = size(v_, 2:max(2, ndims(v_)));

% 6xN array to skew-symmetric matrix
if size(v_, 1) == 6
  s = reshape(vec2skew_6d(reshape(v_, 6, [])), [ 4 , 4, szv ]);

% 3xN vector to skew-symmetric matrix
else
  s = reshape(vec2skew_3d(reshape(v_, 3, [])), [ 3 , 3 , szv ]);
  
end


end


function s = vec2skew_3d(v)
%% VEC2SKEW_3D



% Number of vectors == columns of V
nv = size(v, 2);

% Reshape the vector in the depth dimension
v_ = reshape(v, [3, 1, nv]);

% Quicker access to important parts
n = zeros(1, 1, nv);
x = v_(1,1,:);
y = v_(2,1,:);
z = v_(3,1,:);

% Explicitly define concatenation dimension for codegen
tempS = cat( ...
    1 ...
  , n, -z, +y ...
  , +z, n, -x ...
  , -y, +x, n ...
);

% Convert column vector S into a 3x3xN matrix
s = permute(reshape(tempS, [3, 3, nv]), [2, 1, 3]);


end


function s = vec2skew_6d(v)
%% VEC2SKEW_6D



% Number of samples
nv = size(v, 2);

% Extract two components of V = [ W ; U ]
W = hslice(v, 1, 1:3);
U = hslice(v, 1, 4:6);

% And create skew-symmetrix matrix of a 6xN vectors
s = zeros(4, 4, nv);
s(1:3,1:3,:) = vec2skew_3d(W);
s(1:3,4,:)   = permute(U, [1, 3, 2]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
