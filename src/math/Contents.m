% MATH
%
% Files
%   adjugate        - Calculate the adjugate matrix of A
%   allisclose      - Check if elements are close to each other
%   ang2ddrotm      - Calculate change of rotation matrix
%   ang2drotm       - Calculate change of rotation matrix
%   ang2rotm        - ANG2ROTM
%   angle2rotm      - Create Tait-Bryan rotation matrices from rotation angles
%   bbox            - Calculates the 2D bounding box for the given points
%   bbox3           - Calculates the 3D bounding box for the given points
%   cart2quadrant   - returns the quadrant of the given Cartesian coordinates
%   cbrt            - Cubic root of X
%   chebdiffmtx     - Calculate the Chebyshev Differentiation Matrix
%   chebpts1        - Calculate the Chebyshev nodes of degree K
%   chebpts2        - Calculate the Chebyshev points of degree N
%   closest         - finds the row and column of the matrix element closest to a given
%   collatz         - Return the Collatz number of N
%   crossing        - find the crossings of a given level of a signal
%   derivest        - Estimate the N-th derivative of a fun at X0
%   directionaldiff - estimate directional derivative of a function of n variables
%   evec            - Create a spatial unit vector
%   evec2           - Create a planar unit vector
%   evec3           - Create a spatial unit vector
%   evecn           - Create a unit vector of some dimenions.
%   f2w             - turns ordinary frequency into angular frequency
%   factorial2      - Factorial function.
%   fibonacci       - Fibonacci numbers.
%   fps2t           - Turn an FPS into a time vector
%   gausslegendre   - Determine Gauss-Legendre Quadrature abscissae and weights
%   gcd2            - Greatest common divisor of all elements.
%   gradest         - estimate gradient vector of an analytical function of n variables
%   halfversin      - creates the half versine value
%   hessdiag        - Diagonal elements of the Hessian matrix of a function
%   hessest         - Estimate Hessian matrix of a scalar function
%   isclose         - Determines whether two numbers are close to each other.
%   iseven          - checks the given number(s) for being even
%   isprime2        - True for prime numbers.
%   jacobest        - Estimate Jacobian of a vector-valued function
%   jacobianest     - Estimate Jacobian matrix of a vector-valued function of N variables
%   laginterp       - Multidimensional Lagrange interpolation
%   lcm2            - Least common multiple of all elements.
%   lerp            - Linear interpolation between two values
%   loglim          - returns logarithmic limits
%   maxprime        - Return the largest prime number smaller than N
%   mcols           - Count number of columns of matrix A
%   minprime        - Return the smallest prime number smaller than N
%   mmax            - behaves similar to MAX except that it automatically shrinks down to
%   mmin            - behaves similar to MIN except that it automatically shrinks down to
%   mnormcol        - Normalize a matrix per column
%   mnormdim        - Normalize matrix along a certain dimension
%   mnormrow        - Normalize a matrix per row
%   nextprime       - Return the first prime number larger than integer N
%   nrows           - Count number of rows of matrix A
%   prevprime       - Return the first prime number smaller than integer N
%   randbnd         - Create uniformly distributed pseudorandom numbers on an interval
%   reldiff         - Calculate relative difference of X and Y
%   rot2            - creates the 2D rotation matrix of angles A
%   rot3            - Rotate with an angle about the given axis
%   rotm2ang        - Convert planar rotation matrix to angles
%   rotm2row        - converts a 3D rotation matrix to a row
%   rotmdiff        - determine the difference between two rotation matrices
%   rotrow2m        - converts a 1d rotation matrix row vector to its matrix representation
%   rotx            - Create rotation matrix from rotation about X-axis
%   rotxsym         - Symbolic rotation matrix about the x-axis
%   roty            - Create rotation matrix from rotation about Y-Axis
%   rotysym         - Symbolic rotation matrix about the y-axis
%   rotz            - Create rotation matrix from rotation about Z-Axis
%   rotzsym         - Symbolic rotation matrix about the z-axis
%   runge           - Calculates Runge's function 1 / (1 + x^2) over X
%   sigdigits       - Return significant digits in a numeric non-uint64 number
%   skew2vec        - Convert skew-symmetric matrix into skew-symmetric vector
%   sym_eul2rotm    - Convert symbolic Euler angles to rotation matrix
%   vec2d2skew      - Turn the input into its skew-symmetrix matrix form
%   vec2skew        - VEC2SKEW
%   vec2tens        - converts a vector to its 2d tensor matrix representation.
%   versin          - calculates the versine of the argument
%   w2f             - turns angular frequency into ordinary frequency
%   xaxis           - Return a 3D X-axis vector
%   xaxis2          - XAXIS Return a 2D X-axis vector
%   yaxis           - Return a 3D Y-axis vector
%   yaxis2          - YAXIS Return a 2D Y-axis vector
%   zaxis           - Return a 3D Z-axis vector
