function yq = baryinterp(x, y, xq)
%% BARYINTERP Barycentric interpolation of a polynomial
%
% Inputs:
%
%   X                       Nx1 vector of sample points at which function is
%                           evaluated.
% 
%   Y                       NxK array of data points i.e., Y = FUN(X).
% 
%   XQ                      1xQ vector of query points.
%
% Outputs:
%
%   YQ                      QxK array of interpolated values.



%% File information
% Author: Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% BARYINTERP(X, Y, XQ)
narginchk(0, 3);

% BARYINTERP(___)
% YQ = BARYINTERP(___)
nargoutchk(0, 1);



%% Algorithm

% Number of weights
nw = numel(x) - 1;
% Number of query points
nq = numel(xq);

% Get sample vectors into the right shape
x  = reshape(x, 1, 1, []);
xq = reshape(xq, [], 1);

% Ensure Y is a column vector
if isvector(y)
  y = reshape(y, [], 1);
end

% Calculate coefficients, then push them onto the third dimension
cj = [ 1 / 2 ; ones(nw - 1, 1) ; 1 / 2 ] .* ( ( -1) .^ transpose( 0:nw ) );
cj = permute(cj, [2, 3, 1]);

% Actual barycentric interpolation on three dimensions i.e., first dimension are
% query points, the second dimension the dimension of the data, and the third
% dimension are the node points of X
xdiff = xq - x;
temp = cj ./ xdiff;
numer = temp .* permute(y, [3, 2, 1]);
numer = sum(numer, 3);
denom = sum(temp, 3);
yq = numer ./ denom;

% Handle cases where XQ == Xj
kk = find(reshape(abs(reshape(xq, [], 1) - reshape(x, 1, [])) == 0, [], 1));
[yqe, ye] = ind2sub([nq , nw + 1], kk);
yq(yqe,:) = y(ye,:);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.

