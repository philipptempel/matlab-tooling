function ue = unixepoch()
%% UNIXEPOCH Return the Unix expoch
%
% UE = UNIXEPOCH() returns the Unix epoch as DATETIME object.
%
% Outputs:
%
%   UE                      Unix epoch as DATETIME object.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   DATETIME



%% Parse arguments

% UNIXEPOCH()
narginchk(0, 0);

% UNIXEPOCH(___)
% UE = UNIXEPOCH(___)
nargoutchk(0, 1);



%% Algorithm

% Best to keep this variable as a default since we will not be changing it
persistent ue_

if isempty(ue_)
  ue_ = datetime(0, 'ConvertFrom', 'epochtime');
end

% Assign output quantities
ue = ue_;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
