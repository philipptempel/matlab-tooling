function [x, fval, exitflag, output, jacob] = newtonraphson(fun, x0, options, varargin)%#codegen
%% NEWTONRAPHSON Solve set of non-linear equations using Newton-Raphson method.
%
% X = NEWTONRAPHSON(FUN, X0) FUN is a function handle that returns a vector of
% residuals equations, FUN, and takes a vector, x, as its only argument. When
% the equations are solved by x, then FUN(x) == zeros(size(FUN(:), 1)).
%
% Optionally FUN may return the Jacobian, Jij = dFi/dxj, as an additional
% output. The Jacobian must have the same number of rows as FUN and the same
% number of columns as X. The columns of the Jacobians correspond to d/dxj and
% the rows correspond to dFi/d.
%
% X = NEWTONRAPHSON(FUN, X0, OPTIONS)
%
% [X, FVAL] = NEWTONRAPHSON(FUN, X0, ...)
%
% [X, FVAL, EXITFLAG] = NEWTONRAPHSON(FUN, X0, ...)
%
% [X, FVAL, EXITFLAG, OUTPUT] = NEWTONRAPHSON(FUN, X0, ...)
%
% [X, FVAL, EXITFLAG, OUTPUT, JACOB] = NEWTONRAPHSON(FUN, X0, ...)
%
%   EG:  J23 = dF2/dx3 is the 2nd row ad 3rd column.
%
% If FUN only returns one output, then J is estimated using a center difference
% approximation,
%
%   Jij = dFi/dxj = ( Fi(xj + dx) - Fi(xj - dx) ) / (2 dx );
%
% NOTE: If the Jacobian is not square the system is either over or
% under-constrained.
%
% X0 is a vector of initial guesses.
%
% OPTIONS                   Structure of solver options created using OPTIMSET.
%                           EG: options = optimset('TolX', 0.001).
%
% The following options can be set:
% 
%   TOLFUN                  Maximum tolerance of residual norm.
%                           Default: 1e-6.
%
%   TOLX                    Minimum tolerance of relative maximum stepsize.
%                           Default: 1e-6.
%
%   MAXITER                 Maximum number of iterations before giving up.
%                           Default: 100
%
%   DISPLAY                 Set level of verbosity. Possible values are
%                           * 'off'
%                           * 'iter'
%                           * 'iter-detailed'
%
% X is the solution that solves the set of equations within the given tolerance.
% FVAL is FUN(X). EXITFLAG is an integer that corresponds to the output
% conditions, OUTPUT is a structure containing the number of iterations, the
% final stepsize and exitflag message and JACOB is the J(X).
%
% See also
%   OPTIMSET OPTIMGET FMINSEARCH FZERO FMINBND FSOLVE LSQNONLIN
%
% References
% * http://en.wikipedia.org/wiki/Newton's_method
% * http://en.wikipedia.org/wiki/Newton's_method_in_optimization
% * 9.7 Globally Convergent Methods for Nonlinear Systems of Equations 383,
%   Numerical Recipes in C, Second Edition (1992),
%   http://www.nrbook.com/a/bookcpdf.php
% * https://de.mathworks.com/matlabcentral/fileexchange/43097-newton-raphson-solver



%% File Information
% Author: Philipp Tempel <philipp.tempel@ieee.org>
% Changelog:
%   Version 0.5
%     * allow sparse matrices, replace cond() with condest()
%     * check if Jstar has NaN or Inf, return NaN or Inf for cond() and return
%     exitflag: -1, matrix is singular.
%     * fix bug: max iteration detection and exitflag reporting typos
%
%   Version 0.4
%     * allow lsq curve fitting type problems, IE non-square matrices
%     * exit if J is singular or if dx is NaN or Inf
%
%   Version 0.3
%     * Display RCOND each step.
%     * Replace nargout checking in funwrapper with ducktypin.
%     * Remove Ftyp and F scaling b/c F(typx)->0 & F/Ftyp->Inf!
%     * User Numerical Recipies minimum Newton step, backtracking line search
%     with alpha = 1e-4, min_lambda = 0.1 and max_lambda = 0.5.
%     * Output messages, exitflag and min relative step.
%
%   Version 0.2
%     * Remove `options.FinDiffRelStep` and `options.TypicalX` since not in
%     MATLAB.
%     * Set `dx = eps^(1/3)` in `jacobian` function.
%     * Remove `options` argument from `funwrapper` & `jacobian` functions
%     since no longer needed.
%     * Set typx = x0; typx(x0==0) = 1; % use initial guess as typx, if 0 use 1.
%     * Replace `feval` with `evalf` since `feval` is builtin.



%% Parse Arguments


% NEWTONRAPHSON(FUN, X0)
% NEWTONRAPHSON(FUN, X0, [], ...)
narginchk(2, Inf);

% NEWTONRAPHSON(___)
% X = NEWTONRAPHSON(___)
% [X, FVAL] = NEWTONRAPHSON(___)
% [X, FVAL, EXITFLAG] = NEWTONRAPHSON(___)
% [X, FVAL, EXITFLAG, OUTPUT] = NEWTONRAPHSON(___)
% [X, FVAL, EXITFLAG, OUTPUT, JACOB] = NEWTONRAPHSON(___)
nargoutchk(0, 5);

% NEWTONRAPHSON(FUN, X0)
% NEWTONRAPHSON(FUN, X0, [], ...)
if nargin < 3 || isempty(options)
  options = struct();
end

% wrap FUN so it always returns J
if nargout(fun) == 1
  FUN = @(x, varargin) funwrapper(fun, x, varargin{:});
  
else
  FUN = fun;
  
end



%% Parse options

% set default options
defaultopt = optimset( ...
    'TolX', 1e-12 ...
  , 'TolFun', 1e-6 ...
  , 'MaxIter', 100 ...
  , 'Display', 'iter' ...
);

% Relative max step tolerance
TOLSTEP = optimget(options, 'TolX', defaultopt, 'fast');
% Function tolerance
TOLFUN = optimget(options, 'TolFun', defaultopt, 'fast');
% Max number of iterations
MAXITER = optimget(options, 'MaxIter', defaultopt, 'fast');
% Display iterations
DISPLAY = optimget(options, 'Display', defaultopt, 'fast');
% x scaling value, remove zeros
TYPX = max(abs(x0), 1);

% criteria for decrease
ALPHA = 1e-4;
% min lambda
MIN_LAMBDA = 0.1;
% max lambda
MAX_LAMBDA = 0.5;

% Display function
switch lower(DISPLAY)
  case {'iter', 'iter-detailed'}
    verbosity = 3;
    
  otherwise
    verbosity = 0;
    
end

% Print out header
if verbosity > 0
  header = sprintf([ ...
      '\n     iter   residual   stepnorm     lambda      rcond  convergence ' ...
  ]);
  fmtstr = '%9d %10.4g %10.4g %10.4g %10.4g %12.4g \n';
end



%% Prepare

% needs to be a column vector
szx = size(x0);
x0 = x0(:);

% Scaling values
% TODO: let user set weights
weight = ones(numel(FUN(x0, varargin{:}) ),1);
% Jacobian scaling matrix
J0 = weight .* ( 1 ./ transpose(TYPX) );

% initial guess
x = x0;

% evaluate initial guess
[fval, J] = FUN(x, varargin{:});

% scale Jacobian
Jstar = J ./ J0;

% matrix may be singular
if any( isnan(Jstar(:)) ) || any( isinf(Jstar(:)) )
  exitflag = -1;

% normal exit
else
  exitflag = 1;
  
end

% reciprocal condition
if issparse(Jstar)
  rc = 1 / condest(Jstar);
  
else
  if any( isnan(Jstar(:)) )
    rc = NaN;
    
  elseif any( isinf(Jstar(:)) )
    rc = Inf;
    
  else
    rc = 1 / cond(Jstar);
    
  end
  
end

% calculate norm of the residuals
fnorm = norm(fval);
% dummy values
dx = zeros(size(x0));
convergence = Inf;



%% Solve Equation

% start counter
kiter = 0;
% backtracking
lambda = 1;

if verbosity > 0
  disp(header);
  fprintf(fmtstr, kiter, fnorm, norm(dx), lambda, rc, convergence);
end

% Newton-Raphson loop
while ( fnorm > TOLFUN || lambda < 1 ) ...
    && exitflag >= 0 ...
    && kiter <= MAXITER
  
  % If inside a standard Newton-Raphson loop
  if lambda ==1
    % increment counter
    kiter = kiter + 1;
    % calculate Newton step
    dx_star = -Jstar \ fval;
    % NOTE: use isnan(f) || isinf(f) instead of STPMAX
    % rescale x
    dx = dx_star .* TYPX;
    % gradient of resnorm
    g = transpose(fval) * Jstar;
    % slope of gradient
    slope = g * dx_star;
    % objective function
    fold = transpose(fval) * fval;
    % initial value
    xold = x;
    
    lambda_min = TOLSTEP / max( abs(dx) ./ max(abs(xold), 1) );
    
  end
  
  % x is too close to XOLD
  if lambda < lambda_min
    exitflag = 2;
    
    break
    
  % matrix may be singular
  elseif any( isnan(dx) ) || any( isinf(dx) )
    exitflag = -1;
    
    break
    
  end
  
  % next guess
  x = xold + dx * lambda;
  % evaluate next residuals
  [fval, J] = FUN(x, varargin{:});
  
  % scale next Jacobian
  Jstar = J ./ J0;
  
  % new objective function
  f = transpose(fval) * fval;
  
  %%% check for convergence
  
  % save previous lambda
  lambda1 = lambda;
  if f > fold + ALPHA * lambda * slope
    if lambda == 1
      % calculate lambda
      lambda = -slope / 2 / ( f - fold - slope );
      
    else
      A = 1 / ( lambda1 - lambda2 );
      B = [ ...
        1 / ( lambda1 ^ 2 ) ...
          , -1 / ( lambda2 ^ 2 ) ; ...
        -lambda2 / ( lambda1 ^ 2 ) ...
          , lambda1 / ( lambda2 ^ 2 ) ; ...
        ];
      C = [ ...
        f  - fold - lambda1 * slope ; ...
        f2 - fold - lambda2 * slope ; ...
      ];
      coeff = num2cell(A * B * C);
      [a,b] = coeff{:};
      
      if a == 0
        lambda = -slope / 2 / b;
        
      else
        discriminant = ( b ^ 2 ) - 3 * a * slope;
        
        if discriminant < 0
          lambda = MAX_LAMBDA * lambda1;
          
        elseif b <= 0
          lambda = ( -b + sqrt(discriminant) ) / 3 / a;
          
        else
          lambda = -slope / ( b + sqrt(discriminant) );
          
        end
        
      end
      
      % minimum step length
      lambda = min(lambda, MAX_LAMBDA * lambda1);
      
    end
    
  % limit undefined evaluation or overflow
  elseif isnan(f) || isinf(f)
    lambda = MAX_LAMBDA * lambda1;
    
  % fraction of Newton step
  else
    lambda = 1;
    
  end
  
  if lambda < 1
    % save 2nd most previous value
    lambda2 = lambda1;
    f2 = f;
    % minimum step length
    lambda = max(lambda, MIN_LAMBDA * lambda1);
    
    continue
    
  end
  
  %%% Display
  % old resnorm
  resnorm0 = fnorm;
  % calculate new resnorm
  fnorm = norm(fval);
  % calculate convergence rate
  convergence = log(resnorm0 / fnorm);
  % norm of the step
  stepnorm = norm(dx);
  
  % matrix may be singular
  if any( isnan(Jstar(:)) ) || any( isinf(Jstar(:)) )
    exitflag = -1;
    
    break
    
  end

  % reciprocal condition
  if issparse(Jstar)
    rc = 1 / condest(Jstar);
    
  else
    rc = 1 / cond(Jstar);
    
  end

  if verbosity > 0
    fprintf(fmtstr, kiter, fnorm, stepnorm, lambda1, rc, convergence);
  end
  
end

if kiter >= MAXITER
  exitflag = 0;
end



%% Assign Output

% Return x into its original shape
x = reshape(x, szx);

% Initialize output structure
output = struct();
% final number of iterations
output.iterations = kiter;
% final stepsize
output.stepsize = dx;
% final lambda
output.lambda = lambda;

% Set output message depending on exitflag
switch exitflag
  case 0
    output.message = 'Number of iterations exceeded OPTIONS.MAXITER.';
    
  case 2
    output.message = 'May have converged, but X is too close to XOLD.';
    
  case -1
    output.message = 'Matrix may be singular. Step was NaN or Inf.';
    
  otherwise
    output.message = 'Normal exit.';
    
end

jacob = J;


end


function [F, J] = funwrapper(fun, x, varargin)
%% FUNWRAPPER



F = fun(x, varargin{:});
% Size of Jacobian
szJac = [ numel(F) , numel(x) ];
% evaluate center diff if no Jacobian
J = jacobian(fun, szJac, x, varargin{:});

% needs to be a column vector
F = F(:);


end


function J = jacobian(fun, szJac, x, varargin)
%% JACOBIAN



% finite difference delta
dx = eps^( 1 / 3 );
% degrees of freedom
nx = szJac(2);

% matrix of zeros
J = zeros(szJac);

% Pre-calculate step size
dx2 = 2 * dx;

% Loop over each column
for n = 1:nx
  % create a vector of deltas, change delta_n by dx
  delx    = zeros(nx, 1);
  delx(n) = delx(n) + dx;
  
  % derivatives dF/d_n
  J(:, n) = reshape( fun(x + delx, varargin{:}) - fun(x - delx, varargin{:}), [], 1) / dx2;
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
