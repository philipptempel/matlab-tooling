# MATLAB Tooling

A collection of useful functions and scripts that make working with MATLAB a breeze and even more efficient.

Care to know what this package is providing? Check out the [list of functions](FUNCTIONS.md) giving you a quick overview of all the functions available.


## Getting Started


### Installation

The simplest and quickets way to getting started is by cloning this repository and its submodules

```bash
$ git clone --recursive https://gitlab.com/philipptempel/matlab-tooling.git
```


### Usage

To use the code contained within this repository, either start MATLAB into this project's directory. Or, if MATLAB is already running, change into this project's directory and run the `startup()` function

```matlab
>> cd /path/to/philipptempel/matlab-tooling/
>> startup();
```

What this will do is

1. Get the path definitions for the project from `private/mtl_projpath()`
2. Add the paths required to use this project using MATLAB's built-in `addpath()` method
3. Perform additional setup tasks such as publishing plot styles into the `ExportSetup` directory inside `prefdir(0)`

That's it. You are now officially all set to use any of the functions defind in the [list of functions](FUNCTIONS.md). Happy efficiency.

## Most Used Functions

Here's a list of the top 10 of functions according to heavy MATLAB user @philipptempel

### Data Manipulation

Most used functions/methods inside methods when dealing with data.

1. `restart`
1. `fullpath`
1. `ascol`/`asrow`
1. `tspan`
1. `mkfun`/`mvfun`/`cpfun`
1. `closest`
1. `datestr8601`
1. `progressbar`
1. `bbox`/`bbox3`
1. `evec`/`evec3`

### Data Visualization

Most used methods for data visualization and data import/export

1. `applyplotstyle`
1. `apply(SolarizedPlotTheme.Dark)`
1. `ustutt.linestyles`
1. `ustutt.distcolors`
1. `ustutt.layout`
1. `distinguishableColors`
1. `plot_cyclelinestyles`
1. `plot_markers`
1. `ruler`

## Guides

n/a

## Comments

### On notation of matrices (aka. **row/column issue**)

I like to think of MATLAB dealing with the row/column issue as saying that columns contain different variables, and rows contain different observations of those variables. Thus, the rows might be different observations in time of three different temperatures, which are represented in columns A, B, and C. This is consistent with MATLAB's behaviour for sum, mean, etc., which produce "the sum of all my observations for each variable".

### On `numel` vs `length`

IMHO from a code readability viewpoint, `length` should be used on one-dimensional arrays. It is about "intentional programming", you see the code and understand what programmer had in mind when conceiving his work. So when I see `numel` I know it is used on a matrix.
@see https://stackoverflow.com/questions/3119739/difference-between-matlabs-numel-and-length-functions
